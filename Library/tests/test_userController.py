from unittest import TestCase
from Library.Model.Enums import ValidType


class TestUserController(TestCase):

    @classmethod
    def setUpClass(cls):
        from Library.Controller.UserController import UserController
        from Library.Model.LibraryManager import LibraryManager
        from Library.Model.Enums import MemberStatus, EmployeeStatus

        cls.lm = LibraryManager()
        cls._controller = UserController(cls.lm)
        cls.valid_member_status = MemberStatus.REGULAR
        cls.valid_employee_status = EmployeeStatus.ADMIN

    def test_validate_username(self):
        # Valid data
        self.assertTrue(self._controller.validate_username('marKO12'))
        # Invalid data
        self.assertFalse(self._controller.validate_username('mar%#KO12'))

    def test_validate_password(self):
        # Valid data
        self.assertTrue(self._controller.validate_password('Mark209...'))
        # Invalid data
        self.assertFalse(self._controller.validate_password('Mar@.'))

    def test_validate_name(self):
        self.assertTrue(self._controller.validate_name('Mark'))
        # Invalid data
        self.assertFalse(self._controller.validate_name('Mark24'))

    def test_validate_surname(self):
        self.assertTrue(self._controller.validate_surname('Marks'))
        # Invalid data
        self.assertFalse(self._controller.validate_surname('Marks112'))

    def test_validate_date_of_birth(self):
        # Valid data
        self.assertTrue(self._controller.validate_date_of_birth('1.1.2000.'))
        # Invalid data
        self.assertFalse(self._controller.validate_date_of_birth('1.2.2k1a'))

    def test_validate_jmbg(self):
        # Valid data
        self.assertTrue(self._controller.validate_jmbg('1234567890987'))
        # Invalid data
        self.assertFalse(self._controller.validate_jmbg('121142dasdaty,,'))

    def test_validate_address(self):
        self.assertTrue(self._controller.validate_address('Arse Teodorovica 2a'))
        # Invalid data
        self.assertFalse(self._controller.validate_address('Asrw @002.,'))

    def test_validate_email(self):
        # Valid data
        self.assertTrue(self._controller.validate_email('mark2@gmail.com'))
        # Invalid data
        self.assertFalse(self._controller.validate_email('mark2email.com'))

    def test_validate_member_status(self):
        # Valid data
        self.assertTrue(self._controller.validate_member_status(self.valid_member_status))
        # Invalid data
        self.assertFalse(self._controller.validate_member_status(123132))

    def test_validate_employee_status(self):
        # Valid data
        self.assertTrue(self._controller.validate_employee_status(self.valid_employee_status))
        # Invalid data
        self.assertFalse(self._controller.validate_employee_status(123132))

    def test_validate_text(self):
        self.assertTrue(self._controller.validate_text('Arse Teodorovica 2a'))
        # Invalid data
        self.assertFalse(self._controller.validate_text('Asrw @002.,'))

    def test_validate_isbn(self):
        self.assertTrue(self._controller.validate_isbn('1234-2331233'))
        # Invalid data
        self.assertFalse(self._controller.validate_isbn('123123kjkk'))

    def test_is_num_of_reserved_copies_valid(self):
        self.assertTrue(self._controller.is_num_of_reserved_copies_valid('13', '50'))
        # Invalid data
        self.assertFalse(self._controller.is_num_of_reserved_copies_valid('100', '10'))

    def test_extend_return_date_valid(self):
        targeted_fields = [0]
        ret_val = self.lm.extend_return_date('jovana', targeted_fields)
        if ret_val:
            ret_val = ValidType.LOAN_EXT
        self.assertEqual(ValidType.LOAN_EXT, ret_val)

    def test_extend_return_date_invalid(self):
        targeted_fields = [0]
        ret_val = self.lm.extend_return_date('milan', targeted_fields)
        if ret_val:
            ret_val = ValidType.LOAN_EXT
        self.assertNotEqual(ValidType.LOAN_EXT, ret_val)

    def test_cancel_reservation(self):
        targeted_fields = []
        ret_val = self.lm.cancel_reservation('jov', targeted_fields)
        if ret_val:
            ret_val = ValidType.RES_CAN
        self.assertNotEqual(ValidType.RES_CAN, ret_val)

    def test_reserve_books_valid(self):
        valid = ValidType.BOOK_NOT_RES
        ret_val = self.lm.reserve_book_copies('jovana', 0, 2)
        if ret_val:
            valid = ValidType.BOOK_RES
        self.assertEqual(ValidType.BOOK_RES, valid)

    def test_activate_membership_valid(self):
        ret_val = self._controller.activate_membership('jovana')
        self.assertEqual(ValidType.MEMB_AC, ret_val)

    def test_activate_membership_invalid(self):
        ret_val = self._controller.activate_membership('jovana')
        self.assertNotEqual(ValidType.MEMB_NOT_AC, ret_val)

    def test_extend_membership_valid(self):
        username = 'jovana'
        membership_type = 'Pola godine'
        ret_val = self._controller.extend_membership(username, membership_type)
        self.assertEqual(ValidType.MEMB_EXT, ret_val)

    def test_extend_membership_invalid(self):
        username = 'josdad'
        membership_type = 'Pola godine'
        ret_val = self._controller.extend_membership(username, membership_type)
        self.assertNotEqual(ValidType.MEMB_EXT, ret_val)

    def test_change_password_valid(self):
        user = self.lm.get_user_by_username('jovana')
        old_pass = '1234'  # treba da se promijeni lozinka jer pri izmjeni se mijenja u fajlu
        new_pass = '1234'
        confirmed_pass = '1234'
        ret_val = self._controller.change_password(user, old_pass, new_pass, confirmed_pass)
        self.assertEqual(ValidType.PASS_CH, ret_val)

    def test_change_password_invalid(self):
        user = self.lm.get_user_by_username('jovana')
        old_pass = '123'
        new_pass = '1234'
        confirmed_pass = '1234asdsa'
        ret_val = self._controller.change_password(user, old_pass, new_pass, confirmed_pass)
        self.assertEqual(ValidType.PASS_NOT_CH, ret_val)
