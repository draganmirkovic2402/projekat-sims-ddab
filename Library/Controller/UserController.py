from Library.Model import Member, Enums
from Library.Model.Enums import Genre
from Library.Application.config import RESERVATION_DAYS_LENGTH, DATE_FORMAT
from datetime import date, timedelta, datetime
from Library.Model.Enums import ValidType
import re

EMAIL_PATTERN = "[a-zA-Z0-9]+@[a-zA-Z]+\\.[a-z]+"
NAME_PATTERN = "^[a-zA-Z ]+$"
USERNAME_PATTERN = "^[a-zA-Z0-9_.-]+$"
PASSWORD_PATTERN = "^[a-zA-Z0-9_.-]+$"
ADDRESS_PATTERN = "^[a-zA-Z0-9 ]+$"
TEXT_PATTERN = "^[a-zA-Z0-9 ,.]+$"
ISBN_PATTERN = "^[\\d-]+$"


class UserController(object):

    def __init__(self, lib_manager):
        self._lib_manager = lib_manager

    def add_book(self, book_info, user):
        try:
            book_info["publishing_year"] = int(book_info["publishing_year"])
        except ValueError:
            return ValidType.INV_PUB_YEAR
        try:
            book_info["num_copies"] = int(book_info["num_copies"])
        except ValueError:
            return ValidType.INV_COPIES
        if not self.validate_text_list(book_info["authors"]):
            return ValidType.INV_AUTHORS
        if not self.validate_text(book_info["title"]):
            return ValidType.INV_TITLE
        if not self.validate_text(book_info["publisher"]):
            return ValidType.INV_PUBLISHER
        if not self.validate_text(book_info["book_format"]):
            return ValidType.INV_FORMAT
        if not self.validate_text_list(book_info["key_words"]):
            return ValidType.INV_KEYW
        if not self.validate_text(book_info["additional_notes"]):
            return ValidType.INV_NOTES
        if not self.validate_text(book_info["isbn"]):
            return ValidType.INV_ISBN
        book_info["branch_office"] = user.workplace_pk
        ret_val = self._lib_manager.add_book(book_info)
        return ValidType.BOOK_ADD if ret_val else ValidType.BOOK_NOT_ADD

    def add_copies(self, book_copies, num_copies):
        book_copies = int(book_copies)
        try:
            num_copies = int(num_copies)
            if num_copies < 0:
                return ValidType.INV_COPIES
        except ValueError:
            return ValidType.INV_COPIES
        ret_val = self._lib_manager.add_book_copies(book_copies, num_copies)
        return ValidType.COPIES_ADD if ret_val else ValidType.COPIES_NOT_ADD

    def evident_return(self, rented_copies, user):
        try:
            rented_copies = int(rented_copies)
            if rented_copies < 0:
                return ValidType.INV_RENT
        except ValueError:
            return ValidType.INV_RENT
        branch_office = user.workplace_pk
        ret_val = self._lib_manager.return_books(rented_copies, branch_office)
        return ValidType.LOAD_RET if ret_val else ValidType.LOAD_NOT_RET

    def evident_loan(self, username, book_copies, num_copies):
        if not self.validate_username(username):
            return ValidType.INV_USER
        try:
            num_copies = int(num_copies)
            if num_copies < 0:
                return ValidType.INV_COPIES
        except ValueError:
            return ValidType.INV_COPIES
        ret_val = self._lib_manager.rent_book_copies(username, book_copies, num_copies)
        return ValidType.LOAN_ADD if ret_val else ValidType.LOAN_NOT_ADD

    def register_user(self, user_info):
        if not self.validate_username(user_info["username"]):
            return ValidType.INV_USER
        if not self.validate_password(user_info["password"]):
            return ValidType.INV_PASS
        if not self.validate_name(user_info["name"]):
            return ValidType.INV_NAME
        if not self.validate_surname(user_info["surname"]):
            return ValidType.INV_SUR
        if not self.validate_date_of_birth(user_info["date_of_birth"]):
            return ValidType.INV_DOB
        if not self.validate_jmbg(user_info["jmbg"]):
            return ValidType.INV_JMBG
        if not self.validate_email(user_info["email"]):
            return ValidType.INV_EMAIL
        if not self.validate_address(user_info["street"] + " " + user_info["number"] + " " +
                                     user_info["postal_code"] + " " + user_info["place"]):
            return ValidType.INV_ADR
        return ValidType.MEM_ADD

    def register_member(self, user_info):
        ret_val = self.register_user(user_info)
        if ret_val.number < 0:
            return ret_val
        ret_val = self._lib_manager.create_member(user_info)
        return ValidType.MEM_ADD if ret_val else ValidType.MEM_NOT_ADD

    def register_employee(self, user_info):
        if not self.register_user(user_info):
            return False
        ret_val = self.register_user(user_info)
        if ret_val.number < 0:
            return ret_val
        ret_val = self._lib_manager.create_employee(user_info)
        return ValidType.EMP_ADD if ret_val else ValidType.EMP_NOT_ADD

    def get_branch_offices(self):
        branch_offices = []
        branch_offices_objects = self._lib_manager.get_branch_offices()
        for key in branch_offices_objects.keys():
            row = []
            row.append(branch_offices_objects[key].pk)
            row.append(branch_offices_objects[key].address.street + " " +
                       branch_offices_objects[key].address.number + " " +
                       branch_offices_objects[key].address.postal_code + " " +
                       branch_offices_objects[key].address.name)
            branch_offices.append(row)
        return branch_offices

    def get_user_from_login(self, username, password):
        user = self._lib_manager.get_user(username, password)
        return user

    def change_password(self, user, old_pass, new_pass, confirmed_pass):
        if user.password != old_pass or new_pass != confirmed_pass:
            return ValidType.PASS_NOT_CH
        if not self.validate_password(new_pass):
            return ValidType.PASS_NOT_CH
        ret_val = self._lib_manager.change_password(user.username, new_pass)
        return ValidType.PASS_CH if ret_val else ValidType.PASS_NOT_CH

    def extend_membership(self, username, membership_type):
        if not self.validate_username(username):
            return ValidType.INV_USER
        user = self._lib_manager.get_user_by_username(username)
        if user is None:
            return ValidType.INV_USER
        mem_type = Enums.MembershipType.parse_text(membership_type)
        ret_val = self._lib_manager.extend_membership(username, mem_type)
        return ValidType.MEMB_EXT if ret_val else ValidType.MEMB_NOT_EXT

    def deactivate_membership(self, username):
        if not self.validate_username(username):
            return ValidType.INV_USER
        ret_val = self._lib_manager.deactivate_user(username)
        return ValidType.MEMB_DEAC if ret_val else ValidType.MEMB_NOT_DEAC

    def activate_membership(self, username):
        if not self.validate_username(username):
            return ValidType.INV_USER
        ret_val = self._lib_manager.activate_user(username)
        return ValidType.MEMB_AC if ret_val else ValidType.MEMB_NOT_AC

    def reserve_books(self, user, books_and_num_of_copies):
        failed = False
        if len(books_and_num_of_copies.keys()) == 0:
            return ValidType.BOOK_NOT_SEL
        for key in books_and_num_of_copies.keys():
            ret_val = self._lib_manager.reserve_book_copies(user.username, key, books_and_num_of_copies[key])
            if not ret_val:
                failed = True
        return ValidType.BOOK_RES if not failed else ValidType.BOOK_NOT_RES

    def filter_data(self, title, author, genre, keywords):
        books = []
        data = self._lib_manager.get_book_copies()
        for key in data.keys():
            if title != data[key].book_edition.title and title != '':
                continue
            if author not in data[key].book_edition.authors_to_str() and author != '':
                continue
            if keywords not in data[key].book_edition.key_words and keywords != '':
                continue
            if Genre.parse_text(genre) == data[key].book_edition.genre or genre == 'Svi':
                self._fill_list_with_data(books, data, key)
        return books

    def get_all_books(self):
        books = []
        copies = self._lib_manager.get_book_copies()
        for key in copies.keys():
            self._fill_list_with_data(books, copies, key)
        return books

    def get_branch_office_books(self, workplace):
        books = []
        copies = self._lib_manager.get_branch_office_books(workplace)
        for key in copies.keys():
            self._fill_list_with_data(books, copies, key)
        return books

    def get_all_reserved_books(self, workplace_pk):
        reserved_books = []
        reserved_copies = self._lib_manager.get_all_reserved_copies(workplace_pk)
        for key in reserved_copies.keys():
            row = []
            row.append(str(reserved_copies[key].book_copies_pk))
            row.append(reserved_copies[key].book_title)
            row.append(reserved_copies[key].book_edition.authors_to_str())
            row.append(reserved_copies[key].reservation_date.strftime(DATE_FORMAT))
            row.append(reserved_copies[key].member.username)
            row.append(str(reserved_copies[key].num_copies))
            reserved_books.append(row)
        return reserved_books

    def get_current_rented_books(self, user):
        rented_books = []
        rented_copies = self._lib_manager.get_current_rented_copies(user.username)
        for key in rented_copies.keys():
            row = []
            row.append(str(rented_copies[key].pk))
            row.append(rented_copies[key].book_title)
            row.append(rented_copies[key].book_edition.authors_to_str())
            row.append(rented_copies[key].rent_date.strftime(DATE_FORMAT))
            row.append(rented_copies[key].return_date.strftime(DATE_FORMAT))
            row.append(rented_copies[key].extended)
            rented_books.append(row)
        return rented_books

    def get_all_rented_books(self, user):
        rented_books = []
        rented_copies = self._lib_manager.get_all_rented_copies(user.username)
        for key in rented_copies.keys():
            row = []
            row.append(rented_copies[key].book_title)
            row.append(rented_copies[key].book_edition.authors_to_str())
            row.append(rented_copies[key].rent_date.strftime(DATE_FORMAT))
            rented_books.append(row)
        return rented_books

    def get_current_reserved_books(self, user):
        reserved_books = []
        reserved_copies = self._lib_manager.get_current_reserved_copies(user.username)
        for key in reserved_copies.keys():
            row = []
            row.append(str(reserved_copies[key].pk))
            row.append(reserved_copies[key].book_title)
            row.append(reserved_copies[key].book_edition.authors_to_str())
            row.append(reserved_copies[key].reservation_date.strftime(DATE_FORMAT))
            take_date = reserved_copies[key].reservation_date + timedelta(days=RESERVATION_DAYS_LENGTH)
            row.append(take_date.strftime(DATE_FORMAT))
            reserved_books.append(row)
        return reserved_books

    def extend_return_date(self, user, targeted_fields):
        if len(targeted_fields) == 0:
            return ValidType.LOAN_NOT_EXT
        ret_val = self._lib_manager.extend_return_date(user.username, targeted_fields)
        return ValidType.LOAN_EXT if ret_val else ValidType.LOAN_NOT_EXT

    def cancel_reservation(self, user, targeted_fields):
        if len(targeted_fields) == 0:
            return ValidType.RES_NOT_CAN
        ret_val = self._lib_manager.cancel_reservation(user.username, targeted_fields)
        return ValidType.RES_CAN if ret_val else ValidType.RES_NOT_CAN

    @staticmethod
    def is_num_of_reserved_copies_valid(user_input_num, border):
        if not user_input_num.isnumeric():
            return False
        if int(user_input_num) <= 0 or int(user_input_num) > int(border):
            return False
        return True

    @staticmethod
    def _fill_list_with_data(books, data, key):
        row = []
        row.append(str(data[key].book_edition.pk))
        row.append(data[key].book_edition.title)
        row.append(data[key].book_edition.authors_to_str())
        row.append(data[key].book_edition.publisher)
        row.append(data[key].branch_office.address.address_to_str())
        row.append(str(data[key].book_edition.publishing_year))
        row.append(str(data[key].num_copies - data[key].num_reserved))
        books.append(row)

    @staticmethod
    def validate_username(username):
        if username is not None and re.match(USERNAME_PATTERN, username):
            return True
        return False

    @staticmethod
    def validate_password(password):
        if password is not None and re.search(PASSWORD_PATTERN, password):
            return True
        return False

    @staticmethod
    def validate_name(name):
        if name is not None and re.search(NAME_PATTERN, name):
            return True
        return False

    @staticmethod
    def validate_surname(surname):
        if surname is not None and re.search(NAME_PATTERN, surname):
            return True
        return False

    @staticmethod
    def validate_date_of_birth(date_string):
        try:
            date_of_birth = datetime.strptime(date_string, DATE_FORMAT)
        except ValueError:
            return False
        if date_of_birth is None or date_of_birth > datetime.today():
            return False
        return True

    @staticmethod
    def validate_jmbg(jmbg):
        if jmbg is None or len(jmbg) != 13:
            return False
        elif jmbg.isnumeric() is False:
            return False
        return True

    @staticmethod
    def validate_address(address):
        if address is not None and re.search(ADDRESS_PATTERN, address):
            return True
        return False

    @staticmethod
    def validate_email(email):
        if re.search(EMAIL_PATTERN, email):
            return True
        return False

    @staticmethod
    def validate_member_status(status):
        if isinstance(status, Enums.MemberStatus):
            return True
        return False

    @staticmethod
    def validate_employee_status(status):
        if isinstance(status, Enums.EmployeeStatus):
            return True
        return False

    @staticmethod
    def validate_text(text):
        if text is not None and re.match(TEXT_PATTERN, text):
            return True
        return False

    @staticmethod
    def validate_int(text):
        return isinstance(text, int)

    @staticmethod
    def validate_isbn(text):
        if text is not None and re.match(ISBN_PATTERN, text):
            return True
        return False

    @staticmethod
    def validate_text_list(text_list):
        for text in text_list:
            if text is not None and re.match(TEXT_PATTERN, text):
                continue
            else:
                return False
        return True
