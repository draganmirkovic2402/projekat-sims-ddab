import sys
from PyQt5 import QtWidgets
from PyQt5.QtGui import QIcon

from Library.Application.config import ICON, STYLE
from Library.Model.LibraryManager import LibraryManager
from Library.View.UserView.LoginView import LoginView
from Library.Controller.UserController import UserController


def main():
    lib_manager = LibraryManager()
    user_controller = UserController(lib_manager)
    app = QtWidgets.QApplication(sys.argv)
    widgetStack = QtWidgets.QStackedWidget()
    loginWindow = LoginView(widgetStack, user_controller, lib_manager)
    widgetStack.addWidget(loginWindow)
    widgetStack.setFixedWidth(1044)
    widgetStack.setFixedHeight(551)
    widgetStack.window().setWindowTitle("Biblioteka Bibi")
    app.setWindowIcon(QIcon(ICON.as_posix()))
    with open(STYLE.as_posix(), 'r') as style_file:
        app.setStyleSheet(style_file.read())
    widgetStack.show()

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
