from datetime import datetime
from dateutil.relativedelta import relativedelta

from Library.Application.config import DATE_FORMAT


class RentedCopies(object):

    def __init__(self, info_dict):
        self._pk = info_dict["pk"]
        self._member = [info_dict["member"], None]
        self._book_edition = [info_dict["book_edition"], None]
        self._num_copies = info_dict["num_copies"]
        self._rent_date = datetime.strptime(info_dict["rent_date"], DATE_FORMAT)
        self._return_date = datetime.strptime(info_dict["return_date"], DATE_FORMAT)
        self._extended = info_dict["extended"]

    @property
    def pk(self):
        return self._pk

    @property
    def member_pk(self):
        return self._member[0]

    @property
    def member(self):
        return self._member[1]

    @property
    def book_edition_pk(self):
        return self._book_edition[0]

    @property
    def book_edition(self):
        return self._book_edition[1]

    @property
    def book_title(self):
        return self.book_edition.title

    @property
    def book_authors(self):
        return self.book_edition.authors

    @property
    def rent_date(self):
        return self._rent_date

    @property
    def return_date(self):
        return self._return_date

    @property
    def extended(self):
        return self._extended

    @property
    def num_copies(self):
        return self._num_copies

    @member.setter
    def member(self, m):
        self._member[1] = m

    def extend(self, extend_by_days):
        self._extended = True
        self._return_date += relativedelta(days=extend_by_days)

    @book_edition.setter
    def book_edition(self, be):
        self._book_edition[1] = be

    def is_current_rented(self):
        return datetime.now() < self._return_date

    def to_rented_dict(self):
        rented_dict = {
            "pk": self._pk,
            "member": self._member[1].pk,
            "book_edition": self._book_edition[1].pk,
            "num_copies": self._num_copies,
            "rent_date": self._rent_date.strftime(DATE_FORMAT),
            "return_date": self._return_date.strftime(DATE_FORMAT),
            "extended": self._extended
        }
        return rented_dict
