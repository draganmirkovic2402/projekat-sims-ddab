from Library.Model.Enums import Genre


class BookEdition(object):
    def __init__(self, info_dict):
        self._pk = info_dict["pk"]
        self._authors = info_dict["authors"]
        self._title = info_dict["title"]
        self._publisher = info_dict["publisher"]
        self._publishing_year = info_dict["publishing_year"]
        self._book_format = info_dict["book_format"]
        self._genre = Genre.parse_number(info_dict["genre"])
        self._key_words = info_dict["key_words"]
        self._additional_notes = info_dict["additional_notes"]
        self._isbn = info_dict["isbn"]
        self._is_rentable = info_dict["is_rentable"]

    @property
    def pk(self):
        return self._pk

    @property
    def authors(self):
        return self._authors

    @property
    def title(self):
        return self._title

    @property
    def publisher(self):
        return self._publisher

    @property
    def publishing_year(self):
        return self._publishing_year

    @property
    def book_format(self):
        return self._book_format

    @property
    def genre(self):
        return self._genre

    @property
    def key_words(self):
        return self._key_words

    @property
    def additional_notes(self):
        return self._additional_notes

    @property
    def isbn(self):
        return self._isbn

    @property
    def is_rentable(self):
        return self._is_rentable

    def to_edition_dict(self):
        editions_dict = {
            "pk": self._pk,
            "authors": self._authors,
            "title": self._title,
            "publisher": self._publisher,
            "publishing_year": self._publishing_year,
            "book_format": self._book_format,
            "genre": self._genre.number,
            "key_words": self._key_words,
            "additional_notes": self._additional_notes,
            "isbn": self._isbn,
            "is_rentable": self._is_rentable
        }
        return editions_dict

    def authors_to_str(self):
        output = ''
        for i in range(len(self._authors)):
            if i == len(self._authors) - 1:
                output += str(self._authors[i])
            else:
                output += str(self._authors[i]) + ','
        return output
