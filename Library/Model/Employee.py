from Library.Model.User import User
from Library.Model.Enums import EmployeeStatus


class Employee(User):

    def __init__(self, user_dict, employee_dict):
        super().__init__(user_dict)
        self._status = EmployeeStatus.parse_number(employee_dict["status"])
        self._workplace = [employee_dict["workplace"], None]

    @property
    def status(self):
        return self._status

    @property
    def workplace_pk(self):
        return self._workplace[0]

    @property
    def workplace(self):
        return self._workplace[1]

    @workplace.setter
    def workplace(self, w):
        self._workplace[1] = w

    def to_employee_dict(self):
        user_dict = {
            "status": self._status.number,
            "workplace": self.workplace_pk
        }
        return user_dict
