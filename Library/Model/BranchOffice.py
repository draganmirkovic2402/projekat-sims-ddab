class BranchOffice(object):

    def __init__(self, office_dict):
        self._pk = office_dict["pk"]
        self._address = [office_dict["address"], None]
        self._employees = dict.fromkeys(office_dict["employees"])
        self._stocked_books = dict.fromkeys(office_dict["stocked_books"])

    @property
    def pk(self):
        return self._pk

    @property
    def address_pk(self):
        return self._address[0]

    @property
    def address(self):
        return self._address[1]

    @property
    def employees(self):
        return self._employees

    @property
    def stocked_books(self):
        return self._stocked_books

    @address.setter
    def address(self, a):
        self._address[1] = a

    @stocked_books.setter
    def stocked_books(self, sb):
        self._stocked_books = sb

    def has_stocked_book_edition(self, book_edition_pk):
        for book_copies in self._stocked_books.values():
            if book_copies.book_edition_pk == book_edition_pk:
                return True
        return False

    def add_book_copies(self, book_edition_pk, num_copies):
        for book_copies in self._stocked_books.values():
            if book_copies.book_edition_pk == book_edition_pk:
                book_copies.num_copies += num_copies

    def add_employee(self, employee):
        self._employees[employee.pk] = employee

    def to_office_dict(self):
        office_dict = {
            "pk": self._pk,
            "address": self._address[0],
            "employees": list(self._employees.keys()),
            "stocked_books": list(self._stocked_books.keys())
        }
        return office_dict
