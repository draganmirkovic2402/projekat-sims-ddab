from datetime import datetime
from dateutil.relativedelta import relativedelta

from Library.Application.config import DATE_FORMAT
from Library.Model.Enums import MemberStatus
from Library.Model.User import User


class Member(User):

    def __init__(self, user_dict, member_dict):
        super().__init__(user_dict)
        self._status = MemberStatus.parse_number(member_dict["status"])
        self._membership_date = datetime.strptime(member_dict["membership_date"], DATE_FORMAT)
        self._rented_books = dict.fromkeys(member_dict["rented_books"])
        self._reserved_books = dict.fromkeys(member_dict["reserved_books"])

    @property
    def status(self):
        return self._status

    @property
    def membership_date(self):
        return self._membership_date

    @property
    def rented_books(self):
        return self._rented_books

    @property
    def reserved_books(self):
        return self._reserved_books

    @reserved_books.setter
    def reserved_books(self, rb):
        self._reserved_books = rb

    @rented_books.setter
    def rented_books(self, rb):
        self._rented_books = rb

    def get_current_rented_copies(self):
        current_rented_copies = {}
        for pk in self._rented_books.keys():
            if self._rented_books[pk].is_current_rented():
                current_rented_copies[pk] = self._rented_books[pk]
        return current_rented_copies

    def get_all_rented_copies(self):
        return self._rented_books

    def get_current_reserved_copies(self):
        current_reserved_copies = {}
        for pk in self._reserved_books.keys():
            if self._reserved_books[pk].is_current_reserved():
                current_reserved_copies[pk] = self._reserved_books[pk]
        return current_reserved_copies

    def has_reservation(self, book_copies_pk):
        for reserved_copies in self._reserved_books.values():
            if reserved_copies.book_copies_pk == book_copies_pk:
                return True
        return False

    def get_reserved_book(self, book_copies_pk):
        for reserved_copies in self._reserved_books.values():
            if reserved_copies.book_copies_pk == book_copies_pk:
                return reserved_copies.pk
        return None

    def remove_reservation(self, book_copies_pk):
        for reserved_copies_pk in self._reserved_books.keys():
            if self._reserved_books[reserved_copies_pk].book_copies_pk == book_copies_pk:
                self._reserved_books.pop(reserved_copies_pk)
                break

    def extend_membership(self, months):
        self._membership_date += relativedelta(months=months)
    
    def to_member_dict(self):
        user_dict = {
            "status": self._status.number,
            "membership_date": self.membership_date.strftime(DATE_FORMAT),
            "rented_books": list(self._rented_books.keys()),
            "reserved_books": list(self._reserved_books.keys())
        }
        return user_dict
