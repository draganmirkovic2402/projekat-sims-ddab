from datetime import datetime, timedelta

from Library.Application.config import DATE_FORMAT, RESERVATION_DAYS_LENGTH


class ReservedCopies(object):

    def __init__(self, info_dict):
        self._pk = info_dict["pk"]
        self._member = [info_dict["member"], None]
        self._book_edition = [info_dict["book_edition"], None]
        self._book_copies = [info_dict["book_copies"], None]
        self._num_copies = info_dict["num_copies"]
        self._reservation_date = datetime.strptime(info_dict["reservation_date"], DATE_FORMAT)

    @property
    def pk(self):
        return self._pk

    @property
    def member_pk(self):
        return self._member[0]

    @property
    def member(self):
        return self._member[1]

    @property
    def book_edition_pk(self):
        return self._book_edition[0]

    @property
    def book_edition(self):
        return self._book_edition[1]

    @property
    def book_copies_pk(self):
        return self._book_copies[0]

    @property
    def book_copies(self):
        return self._book_copies[1]

    @property
    def book_title(self):
        return self.book_edition.title

    @property
    def book_authors(self):
        return self.book_edition.authors

    @property
    def reservation_date(self):
        return self._reservation_date

    @property
    def num_copies(self):
        return self._num_copies

    @member.setter
    def member(self, m):
        self._member[1] = m

    @book_edition.setter
    def book_edition(self, be):
        self._book_edition[1] = be

    @book_copies.setter
    def book_copies(self, bc):
        self._book_copies[1] = bc

    def is_current_reserved(self):
        return datetime.now() < self._reservation_date + timedelta(days=RESERVATION_DAYS_LENGTH)

    def to_reserved_dict(self):
        reserved_dict = {
            "pk": self._pk,
            "member": self._member[0],
            "book_edition": self._book_edition[0],
            "book_copies": self._book_copies[0],
            "num_copies": self._num_copies,
            "reservation_date": self._reservation_date.strftime(DATE_FORMAT)
        }
        return reserved_dict
