class BookCopies(object):

    def __init__(self, info_dict):
        self._pk = info_dict["pk"]
        self._branch_office = [info_dict["branch_office"], None]
        self._book_edition = [info_dict["book_edition"], None]
        self._num_copies = info_dict["num_copies"]
        self._num_reserved = info_dict["num_reserved"]

    @property
    def pk(self):
        return self._pk

    @property
    def branch_office_pk(self):
        return self._branch_office[0]

    @property
    def branch_office(self):
        return self._branch_office[1]

    @property
    def book_edition_pk(self):
        return self._book_edition[0]

    @property
    def book_edition(self):
        return self._book_edition[1]

    @property
    def num_copies(self):
        return self._num_copies

    @property
    def num_reserved(self):
        return self._num_reserved

    @branch_office.setter
    def branch_office(self, m):
        self._branch_office[1] = m

    @book_edition.setter
    def book_edition(self, be):
        self._book_edition[1] = be

    @num_copies.setter
    def num_copies(self, nc):
        self._num_copies = nc

    @num_reserved.setter
    def num_reserved(self, nr):
        self._num_reserved = nr

    def to_copies_dict(self):
        copies_dict = {
            "pk": self._pk,
            "branch_office": self._branch_office[0],
            "book_edition": self._book_edition[0],
            "num_copies": self._num_copies,
            "num_reserved": self._num_reserved
        }
        return copies_dict
