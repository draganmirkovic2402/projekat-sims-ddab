import datetime
from dateutil.relativedelta import relativedelta
import json

from Library.Application.config import USERS_PATH, EMPLOYEES_PATH, MEMBERS_PATH, OFFICES_PATH, ADDRESSES_PATH, \
    RENTED_PATH, RESERVED_PATH, COPIES_PATH, EDITIONS_PATH, DATE_FORMAT
from Library.Model.Enums import UserType, MembershipType
from Library.Model.Employee import Employee
from Library.Model.Member import Member
from Library.Model.BranchOffice import BranchOffice
from Library.Model.Address import Address
from Library.Model.BookEdition import BookEdition
from Library.Model.BookCopies import BookCopies
from Library.Model.RentedCopies import RentedCopies
from Library.Model.ReservedCopies import ReservedCopies


class LibraryManager(object):

    def __init__(self):
        self._users_by_username = {}
        self._users = {}
        self._branch_offices = {}
        self._addresses = {}
        self._book_copies = {}
        self._rented_copies = {}
        self._reserved_copies = {}
        self._book_editions = {}
        self._load_database()
        self._load_associations()

    def _create_employee(self, user_dict, employee_dict):
        employee = Employee(user_dict, employee_dict)
        pk = user_dict["pk"]
        self._users[pk] = employee
        username = user_dict["username"]
        self._users_by_username[username] = employee

    def _create_member(self, user_dict, member_dict):
        member = Member(user_dict, member_dict)
        pk = user_dict["pk"]
        self._users[pk] = member
        username = user_dict["username"]
        self._users_by_username[username] = member

    def _load_users(self):
        with open(USERS_PATH, "r") as users_file:
            users_data = json.load(users_file)
            for user_dict in users_data.values():
                pk = user_dict["pk"]
                user_type = UserType(user_dict["user_type"])
                if user_type == UserType.EMPLOYEE:
                    with open(EMPLOYEES_PATH, "r") as employees_file:
                        employees_data = json.load(employees_file)
                        employee_dict = employees_data[str(pk)]
                        self._create_employee(user_dict, employee_dict)
                if user_type == UserType.MEMBER:
                    with open(MEMBERS_PATH, "r") as members_file:
                        members_data = json.load(members_file)
                        member_dict = members_data[str(pk)]
                        self._create_member(user_dict, member_dict)

    @staticmethod
    def _load_data(file_path, class_name):
        loaded_data_dict = {}
        with open(file_path, "r") as data_file:
            data = json.load(data_file)
            for data_dict in data.values():
                pk = data_dict["pk"]
                loaded_data_dict[pk] = class_name(data_dict)
        return loaded_data_dict

    def _load_database(self):
        self._load_users()
        self._branch_offices = self._load_data(OFFICES_PATH, BranchOffice)
        self._addresses = self._load_data(ADDRESSES_PATH, Address)
        self._book_copies = self._load_data(COPIES_PATH, BookCopies)
        self._rented_copies = self._load_data(RENTED_PATH, RentedCopies)
        self._reserved_copies = self._load_data(RESERVED_PATH, ReservedCopies)
        self._book_editions = self._load_data(EDITIONS_PATH, BookEdition)

    def _load_user_associations(self):
        for user in self._users.values():
            address = self._addresses.get(user.address_pk)
            user.address = address
            if user.user_type == UserType.MEMBER:
                for rented_books_pk in user.rented_books.keys():
                    user.rented_books[rented_books_pk] = self._rented_copies.get(rented_books_pk)
                for reserved_books_pk in user.reserved_books.keys():
                    user.reserved_books[reserved_books_pk] = self._reserved_copies.get(reserved_books_pk)
            if user.user_type == UserType.EMPLOYEE:
                user.workplace = self._branch_offices.get(user.workplace_pk)

    def _load_branch_office_associations(self):
        for branch_office in self._branch_offices.values():
            address = self._addresses.get(branch_office.address_pk)
            branch_office.address = address
            for employee_pk in branch_office.employees.keys():
                branch_office.employees[employee_pk] = self._users.get(employee_pk)
            for stocked_books_pk in branch_office.stocked_books.keys():
                branch_office.stocked_books[stocked_books_pk] = self._book_copies.get(stocked_books_pk)

    def _load_book_copies_associations(self):
        for book_copies in self._book_copies.values():
            branch_office = self._branch_offices.get(book_copies.branch_office_pk)
            book_copies.branch_office = branch_office
            book_edition = self._book_editions.get(book_copies.book_edition_pk)
            book_copies.book_edition = book_edition

    def _load_rented_copies_associations(self):
        for rented_copies in self._rented_copies.values():
            member = self._users.get(rented_copies.member_pk)
            rented_copies.member = member
            book_edition = self._book_editions.get(rented_copies.book_edition_pk)
            rented_copies.book_edition = book_edition

    def _load_reserved_copies_associations(self):
        for reserved_copies in self._reserved_copies.values():
            member = self._users.get(reserved_copies.member_pk)
            reserved_copies.member = member
            book_edition = self._book_editions.get(reserved_copies.book_edition_pk)
            reserved_copies.book_edition = book_edition
            book_copies = self._book_copies.get(reserved_copies.book_copies_pk)
            reserved_copies.book_copies = book_copies

    def _load_associations(self):
        self._load_user_associations()
        self._load_branch_office_associations()
        self._load_book_copies_associations()
        self._load_rented_copies_associations()
        self._load_reserved_copies_associations()

    def _branch_offices_to_json(self):
        json_offices = {}
        for pk in self._branch_offices.keys():
            json_offices[str(pk)] = self._branch_offices[pk].to_office_dict()
        return json_offices

    def _save_branch_offices(self):
        with open(OFFICES_PATH, "w") as offices_file:
            json.dump(self._branch_offices_to_json(), offices_file, indent=4)

    def _users_to_json(self):
        json_users = {}
        for pk in self._users.keys():
            json_users[str(pk)] = self._users[pk].to_user_dict()
        return json_users

    def _save_users(self):
        with open(USERS_PATH, "w") as users_file:
            json.dump(self._users_to_json(), users_file, indent=4)

    def _addresses_to_json(self):
        json_addresses = {}
        for pk in self._addresses.keys():
            json_addresses[str(pk)] = self._addresses[pk].to_address_dict()
        return json_addresses

    def _save_addresses(self):
        with open(ADDRESSES_PATH, "w") as addresses_file:
            json.dump(self._addresses_to_json(), addresses_file, indent=4)

    def _employees_to_json(self):
        json_employees = {}
        for pk in self._users.keys():
            if self._users[pk].user_type == UserType.EMPLOYEE:
                json_employees[str(pk)] = self._users[pk].to_employee_dict()
        return json_employees

    def _save_employees(self):
        with open(EMPLOYEES_PATH, "w") as employees_file:
            json.dump(self._employees_to_json(), employees_file, indent=4)

    def _members_to_json(self):
        json_members = {}
        for pk in self._users.keys():
            if self._users[pk].user_type == UserType.MEMBER:
                json_members[str(pk)] = self._users[pk].to_member_dict()
        return json_members

    def _save_members(self):
        with open(MEMBERS_PATH, "w") as members_file:
            json.dump(self._members_to_json(), members_file, indent=4)

    def _editions_to_json(self):
        json_editions = {}
        for pk in self._book_editions.keys():
            json_editions[str(pk)] = self._book_editions[pk].to_edition_dict()
        return json_editions

    def _save_book_editions(self):
        with open(EDITIONS_PATH, "w") as editions_file:
            json.dump(self._editions_to_json(), editions_file, indent=4)

    def _copies_to_json(self):
        json_copies = {}
        for pk in self._book_copies.keys():
            json_copies[str(pk)] = self._book_copies[pk].to_copies_dict()
        return json_copies

    def _save_book_copies(self):
        with open(COPIES_PATH, "w") as copies_file:
            json.dump(self._copies_to_json(), copies_file, indent=4)

    def _rented_to_json(self):
        json_rented = {}
        for pk in self._rented_copies.keys():
            json_rented[str(pk)] = self._rented_copies[pk].to_rented_dict()
        return json_rented

    def _save_rented_copies(self):
        with open(RENTED_PATH, "w") as rented_file:
            json.dump(self._rented_to_json(), rented_file, indent=4)

    def _reserved_to_json(self):
        json_reserved = {}
        for pk in self._reserved_copies.keys():
            json_reserved[str(pk)] = self._reserved_copies[pk].to_reserved_dict()
        return json_reserved

    def _save_reserved_copies(self):
        with open(RESERVED_PATH, "w") as reserved_file:
            json.dump(self._reserved_to_json(), reserved_file, indent=4)

    def get_branch_offices(self):
        return self._branch_offices

    def get_book_copies(self):
        return self._book_copies

    def get_book_copies_number(self, book_copies_pk):
        book_copies = self._book_copies.get(book_copies_pk)
        if book_copies is not None:
            return book_copies.num_copies
        return None

    def get_branch_office_books(self, branch_office):
        return self._branch_offices[branch_office].stocked_books

    def get_user(self, username, password):
        user = self._users_by_username.get(username)
        if user is not None and user.password == password:
            return user
        return None

    def get_user_by_username(self, username):
        user = self._users_by_username.get(username)
        return user

    def get_current_rented_copies(self, username):
        user = self._users_by_username.get(username)
        return user.get_current_rented_copies()

    def get_all_rented_copies(self, username):
        user = self._users_by_username.get(username)
        return user.get_all_rented_copies()

    def get_all_reserved_copies(self, branch_office_pk):
        branch_office = self._branch_offices.get(branch_office_pk)
        if branch_office is not None:
            all_reserved_copies = {}
            for reserved_copies in self._reserved_copies.values():
                book_copies = reserved_copies.book_copies
                if book_copies.branch_office_pk == branch_office_pk:
                    all_reserved_copies[reserved_copies.pk] = reserved_copies
            return all_reserved_copies
        return None

    def get_current_reserved_copies(self, username):
        user = self._users_by_username.get(username)
        return user.get_current_reserved_copies()

    def change_password(self, username, new_password):
        user = self._users_by_username.get(username)
        if user is None:
            return False
        user.password = new_password
        self._save_users()
        return True

    def deactivate_user(self, username):
        user = self._users_by_username.get(username)
        if user is None:
            return False
        user.deactivate()
        self._save_users()
        return True

    def activate_user(self, username):
        user = self._users_by_username.get(username)
        if user is None:
            return False
        user.activate()
        self._save_users()
        return True

    def extend_membership(self, username, membership_type: MembershipType):
        user = self._users_by_username.get(username)
        if user is None:
            return False
        user.extend_membership(membership_type.months)
        self._save_members()
        return True

    def extend_return_date(self, username, rented_copies_pks):
        user = self._users_by_username.get(username)
        if user is None:
            return False
        extend_by_days = user.status.rent_days
        for pk in rented_copies_pks:
            rented_copies = user.rented_books[pk]
            rented_copies.extend(extend_by_days)
        self._save_rented_copies()
        return True

    def cancel_reservation(self, username, reserved_copies_pks):
        user = self._users_by_username.get(username)
        if user is None:
            return False
        for pk in reserved_copies_pks:
            reserved_books = user.reserved_books.get(pk)
            if reserved_books is None:
                return False
            num_reserved = reserved_books.num_copies
            reserved_books.book_copies.num_reserved -= num_reserved
            user.reserved_books.pop(pk)
            self._reserved_copies.pop(pk)
        self._save_reserved_copies()
        self._save_book_copies()
        self._save_members()
        return True

    def add_book_copies(self, book_copies_pk, num_copies):
        book_copies = self._book_copies.get(book_copies_pk)
        if book_copies is None:
            return False
        book_copies.num_copies += num_copies
        self._save_book_copies()
        return True

    def return_books(self, rented_copies_pk, branch_office_pk):
        rented_copies = self._rented_copies.get(rented_copies_pk)
        if rented_copies is None:
            return False
        branch_office = self._branch_offices.get(branch_office_pk)
        if branch_office is None:
            return False
        num_copies = rented_copies.num_copies
        book_edition = rented_copies.book_edition
        if branch_office.has_stocked_book_edition(book_edition.pk):
            branch_office.add_book_copies(book_edition.pk, num_copies)
        else:
            copies_dict = {
                "pk": max(self._book_copies.keys())+1 if len(self._book_copies) > 0 else 0,
                "branch_office": branch_office.pk,
                "book_edition": book_edition.pk,
                "num_copies": num_copies,
                "num_reserved": 0
            }
            book_copies = BookCopies(copies_dict)
            book_copies.branch_office = branch_office
            book_copies.book_edition = book_edition
            branch_office.stocked_books[book_copies.pk] = book_copies
            self._book_copies[book_copies.pk] = book_copies
        user = rented_copies.member
        user.rented_books.pop(rented_copies.pk)
        self._rented_copies.pop(rented_copies.pk)
        self._save_users()
        self._save_rented_copies()
        self._save_book_copies()
        self._save_branch_offices()
        return True

    def create_address(self, info_dict):
        address_dict = {
            "pk": max(self._addresses.keys())+1 if len(self._addresses) > 0 else 0,
            "place": info_dict["place"],
            "postal_code": info_dict["postal_code"],
            "street": info_dict["street"],
            "number": info_dict["number"]
        }
        new_address = Address(address_dict)
        self._addresses[new_address.pk] = new_address
        self._save_addresses()
        return True

    def create_employee(self, info_dict):
        self.create_address(info_dict)
        address_pk = max(self._addresses.keys())
        user_dict = {
            "pk": max(self._users.keys())+1 if len(self._users) > 0 else 0,
            "username": info_dict["username"],
            "password": info_dict["password"],
            "active": True,
            "name": info_dict["name"],
            "surname": info_dict["surname"],
            "date_of_birth": info_dict["date_of_birth"],
            "jmbg": info_dict["jmbg"],
            "address": address_pk,
            "email": info_dict["email"],
            "user_type": UserType.EMPLOYEE.value
        }
        employee_dict = {
            "status": info_dict["status"].number,
            "workplace": info_dict["workplace"]
        }
        new_employee = Employee(user_dict, employee_dict)
        new_employee.address = self._addresses[address_pk]
        new_employee.workplace = self._branch_offices[new_employee.workplace_pk]
        new_employee.workplace.add_employee(new_employee)
        self._users[new_employee.pk] = new_employee
        self._users_by_username[new_employee.username] = new_employee
        self._save_users()
        self._save_employees()
        return True

    def create_member(self, info_dict):
        self.create_address(info_dict)
        address_pk = max(self._addresses.keys())
        user_dict = {
            "pk": max(self._users.keys())+1 if len(self._users) > 0 else 0,
            "username": info_dict["username"],
            "password": info_dict["password"],
            "active": True,
            "name": info_dict["name"],
            "surname": info_dict["surname"],
            "date_of_birth": info_dict["date_of_birth"],
            "jmbg": info_dict["jmbg"],
            "address": address_pk,
            "email": info_dict["email"],
            "user_type": UserType.MEMBER.value
        }
        member_dict = {
            "status": info_dict["status"].number,
            "membership_date": info_dict["membership_date"],
            "rented_books": [],
            "reserved_books": []
        }
        new_member = Member(user_dict, member_dict)
        new_member.address = self._addresses[address_pk]
        self._users[new_member.pk] = new_member
        self._users_by_username[new_member.username] = new_member
        self._save_users()
        self._save_members()
        return True

    def add_book(self, info_dict):
        edition_pk = max(self._book_editions.keys())+1 if len(self._book_editions) > 0 else 0
        edition_dict = {
            "pk": edition_pk,
            "authors": info_dict["authors"],
            "title": info_dict["title"],
            "publisher": info_dict["publisher"],
            "publishing_year": info_dict["publishing_year"],
            "book_format": info_dict["book_format"],
            "genre": info_dict["genre"].number,
            "key_words": info_dict["key_words"],
            "additional_notes": info_dict["additional_notes"],
            "isbn": info_dict["isbn"],
            "is_rentable": True if info_dict["num_copies"] > 0 else False
        }
        copies_dict = {
            "pk": max(self._book_copies.keys())+1 if len(self._book_copies) > 0 else 0,
            "branch_office": info_dict["branch_office"],
            "book_edition": edition_pk,
            "num_copies": info_dict["num_copies"],
            "num_reserved": 0
        }
        edition = BookEdition(edition_dict)
        copies = BookCopies(copies_dict)
        branch_office = self._branch_offices[info_dict["branch_office"]]
        copies.branch_office = branch_office
        branch_office.stocked_books[copies.pk] = copies
        copies.book_edition = edition
        self._book_editions[edition.pk] = edition
        self._book_copies[copies.pk] = copies
        self._save_book_editions()
        self._save_book_copies()
        self._save_branch_offices()
        return True

    def reserve_book_copies(self, username, book_copies_pk, num_copies):
        user = self._users_by_username[username]
        book_copies = self._book_copies[book_copies_pk]
        book_edition = self._book_editions[book_copies.book_edition_pk]
        book_copies.num_reserved += num_copies
        reserved_dict = {
            "pk": max(self._reserved_copies.keys())+1 if len(self._reserved_copies) > 0 else 0,
            "member": user.pk,
            "book_edition": book_edition.pk,
            "book_copies": book_copies.pk,
            "num_copies": num_copies,
            "reservation_date": datetime.datetime.now().strftime(DATE_FORMAT)
        }
        reserved_copies = ReservedCopies(reserved_dict)
        reserved_copies.member = user
        reserved_copies.book_edition = book_edition
        reserved_copies.book_copies = book_copies
        user.reserved_books[reserved_copies.pk] = reserved_copies
        self._reserved_copies[reserved_copies.pk] = reserved_copies
        self._save_members()
        self._save_reserved_copies()
        self._save_book_copies()
        return True

    def rent_book_copies(self, username, book_copies_pk, num_copies):
        user = self._users_by_username[username]
        book_copies = self._book_copies[book_copies_pk]
        book_edition = book_copies.book_edition
        book_copies.num_copies -= num_copies

        if user.has_reservation(book_copies_pk):
            book_copies.num_reserved -= num_copies
            reserved_copies_pk = user.get_reserved_book(book_copies_pk)
            user.remove_reservation(book_copies_pk)
            self._reserved_copies.pop(reserved_copies_pk)
        if book_copies.num_copies == 0:
            branch_office = self._branch_offices[book_copies.branch_office_pk]
            branch_office.stocked_books.pop(book_copies_pk)
            self._book_copies.pop(book_copies_pk)
            self._save_branch_offices()
        return_date = datetime.datetime.now() + relativedelta(days=user.status.rent_days)
        rented_dict = {
            "pk": max(self._rented_copies.keys()) + 1 if len(self._rented_copies) > 0 else 0,
            "member": user.pk,
            "book_edition": book_edition.pk,
            "num_copies": num_copies,
            "rent_date": datetime.datetime.now().strftime(DATE_FORMAT),
            "return_date": return_date.strftime(DATE_FORMAT),
            "extended": False
        }
        rented_copies = RentedCopies(rented_dict)
        rented_copies.member = user
        rented_copies.book_edition = book_edition
        self._rented_copies[rented_copies.pk] = rented_copies
        user.rented_books[rented_copies.pk] = rented_copies
        self._save_book_copies()
        self._save_reserved_copies()
        self._save_rented_copies()
        self._save_members()
        return True


if __name__ == "__main__":
    lm = LibraryManager()
    print()
