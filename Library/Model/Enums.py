from enum import Enum


class MemberStatus(Enum):
    REGULAR = (0, "Regularan", 15)
    CHILD = (1, "Dete", 15)
    PUPIL = (2, "Učenik", 15)
    STUDENT = (3, "Student", 15)
    RETIREE = (4, "Penzioner", 21)
    HONORARY = (5, "Počasni član", 30)

    def __init__(self, number, text, rent_days):
        self._number = number
        self._text = text
        self._rent_days = rent_days

    @property
    def number(self):
        return self._number

    @property
    def text(self):
        return self._text

    @property
    def rent_days(self):
        return self._rent_days

    @classmethod
    def parse_number(cls, number):
        for status in cls.__members__.values():
            if status.number == number:
                return status
        return None

    @classmethod
    def parse_text(cls, text):
        for status in cls.__members__.values():
            if status.text == text:
                return status
        return None

    @classmethod
    def get_types(cls):
        names = []
        for genre in cls.__members__.values():
            names.append(genre.text)
        return names


class EmployeeStatus(Enum):
    ADMIN = (0, "Administrator")
    MODERATOR = (1, "Moderator")
    COUNTER_EMP = (2, "Pult")
    PROCESS_EMP = (3, "Obrada")

    def __init__(self, number, text):
        self._number = number
        self._text = text

    @property
    def number(self):
        return self._number

    @property
    def text(self):
        return self._text

    @classmethod
    def parse_number(cls, number):
        for status in cls.__members__.values():
            if status.number == number:
                return status
        return None

    @classmethod
    def parse_text(cls, text):
        for status in cls.__members__.values():
            if status.text == text:
                return status
        return None

    @classmethod
    def get_types(cls):
        names = []
        for genre in cls.__members__.values():
            names.append(genre.text)
        return names


class MembershipType(Enum):
    HALF_YEAR = (0, "Pola godine", 6)
    YEAR = (1, "Godinu dana", 12)

    def __init__(self, number, text, months):
        self._number = number
        self._text = text
        self._months = months

    @property
    def number(self):
        return self._number

    @property
    def text(self):
        return self._text

    @property
    def months(self):
        return self._months

    @classmethod
    def parse_number(cls, number):
        for status in cls.__members__.values():
            if status.number == number:
                return status
        return None

    @classmethod
    def parse_text(cls, text):
        for status in cls.__members__.values():
            if status.text == text:
                return status
        return None


class UserType(Enum):
    EMPLOYEE = 0
    MEMBER = 1


class ValidType(Enum):
    EMP_ADD = (1, "Dodavanje Zaposlenog", "Uspešno dodat korisnik")
    MEM_ADD = (2, "Dodavanje člana", "Uspešno dodat član")
    COPIES_ADD = (3, "Dodavanje primeraka", "Uspešno dodati primerci")
    BOOK_ADD = (4, "Dodavanje knjige", "Uspešno dodata knjiga")
    MEMB_DEAC = (5, "Deaktivacija članstva", "Uspešno deaktivirano članstvo")
    LOAN_ADD = (6, "Evidentiranje pozajmice", "Uspešno evidentirana pozajmica")
    LOAD_RET = (7, "Evidentiranje vraćanja knjige", "Uspešno evidentirano vraćanje knjige")
    MEMB_EXT = (8, "Produženje članstva", "Uspešno produženo članstvo")
    LOAN_EXT = (9, "Produženje pozajmice", "Uspešno produžena pozajmica")
    BOOK_RES = (10, "Rezervacija knjiga", "Uspešno rezervisane knjige")
    RES_CAN = (11, "Otkazivanje rezervacije", "Uspešno otkazana rezervacija")
    PASS_CH = (12, "Menjanje šifre", "Uspešno izmenjena šifra")
    LOGIN_SUCC = (13, "Prijava", "Uspešna prijava na sistem")
    MEMB_AC = (14, "Deaktivacija članstva", "Uspešno aktivirano članstvo")
    EMP_NOT_ADD = (-1, "Dodavanje Zaposlenog", "Neuspešno dodat korisnik")
    MEM_NOT_ADD = (-2, "Dodavanje člana", "Neuspešno dodat član")
    COPIES_NOT_ADD = (-3, "Dodavanje primeraka", "Neuspešno dodati primerci")
    BOOK_NOT_ADD = (-4, "Dodavanje knjige", "Neuspešno dodata knjiga")
    MEMB_NOT_DEAC = (-5, "Deaktivacija članstva", "Neuspešno deaktivirano članstvo")
    LOAN_NOT_ADD = (-6, "Evidentiranje pozajmice", "Neuspešno evidentirana pozajmica")
    LOAD_NOT_RET = (-7, "Evidentiranje vraćanja knjige", "Neuspešno evidentirano vraćanje knjige")
    MEMB_NOT_EXT = (-8, "Produženje članstva", "Neuspešno produženo članstvo")
    LOAN_NOT_EXT = (-9, "Produženje pozajmice", "Neuspešno produžena pozajmica")
    BOOK_NOT_RES = (-10, "Rezervacija knjiga", "Neuspešno rezervisane knjige")
    RES_NOT_CAN = (-11, "Otkazivanje rezervacije", "Neuspešno otkazana rezervacija")
    PASS_NOT_CH = (-12, "Menjanje šifre", "Neuspešno izmenjena šifra")
    INV_USER = (-13, "Greška", "Neispravno korisničko ime")
    INV_PASS = (-14, "Greška", "Neispravna šifra")
    INV_NAME = (-15, "Greška", "Neispravno ime")
    INV_SUR = (-16, "Greška", "Neispravno prezime")
    INV_DOB = (-17, "Greška", "Neispravan datum rođenja")
    INV_JMBG = (-18, "Greška", "Neispravan jmbg")
    INV_ADR = (-19, "Greška", "Neispravna adresa")
    INV_EMAIL = (-20, "Greška", "Neispravan e-mail adresa")
    BRANCH_NOT_SEL = (-21, "Greška", "Nije selektovana filijala")
    LOGIN_NOT_SUCC = (-22, "Prijava", "Neuspešna prijava na sistem")
    INV_AUTHORS = (-23, "Greška", "Neispravna imena autora")
    INV_TITLE = (-24, "Greška", "Neispravan naslov")
    INV_PUBLISHER = (-25, "Greška", "Neispravan izdavač")
    INV_PUB_YEAR = (-26, "Greška", "Neispravna godina izdavanja")
    INV_FORMAT = (-27, "Greška", "Neispravan format knjige")
    INV_KEYW = (-28, "Greška", "Neispravne ključne reči")
    INV_NOTES = (-29, "Greška", "Neispravan opis")
    INV_ISBN = (-30, "Greška", "Neispravan ISBN")
    INV_COPIES = (-31, "Greška", "Neispravan broj primeraka")
    INV_BOOKID = (-32, "Greška", "Neispravan ID knjige")
    BOOK_NOT_SEL = (-33, "Greška", "Nije selektovana knjiga")
    INV_RENT = (-34, "Greška", "Neispravan ID pozajmice")
    MEMB_NOT_AC = (-35, "Deaktivacija članstva", "Neuspešno aktivirano članstvo")

    def __init__(self, number, title, text):
        self._number = number
        self._title = title
        self._text = text

    @property
    def number(self):
        return self._number

    @property
    def text(self):
        return self._text

    @property
    def title(self):
        return self._title

    @classmethod
    def parse_number(cls, number):
        for status in cls.__members__.values():
            if status.number == number:
                return status
        return None

    @classmethod
    def parse_text(cls, text):
        for status in cls.__members__.values():
            if status.text == text:
                return status
        return None

    @classmethod
    def parse_title(cls, title):
        for status in cls.__members__.values():
            if status.title == title:
                return status
        return None


class Genre(Enum):
    ALL_GENRES = (0, "Svi")
    MYSTERY = (1, "Misterija")
    ADVENTURE = (2, "Avantura")
    POETRY = (3, "Poezija")
    FABLE = (4, "Basna")
    FAIRY_TALE = (5, "Bajka")
    ROMANCE = (6, "Romantika")
    HORROR = (7, "Horor")
    THRILLER = (8, "Triler")
    HISTORY = (9, "Istorija")
    SCIENCE = (10, "Nauka")
    MYTH = (11, "Mitologija")
    BIOGRAPHY = (12, "Biografija")
    AUTOBIOGRAPHY = (13, "Autobiografija")
    FANTASY = (14, "Fantazija")
    WESTERN = (15, "Vestern")

    def __init__(self, number, text):
        self._number = number
        self._text = text

    @property
    def number(self):
        return self._number

    @property
    def text(self):
        return self._text

    @classmethod
    def parse_number(cls, number):
        for status in cls.__members__.values():
            if status.number == number:
                return status
        return None

    @classmethod
    def parse_text(cls, text):
        for status in cls.__members__.values():
            if status.text == text:
                return status
        return None

    @classmethod
    def get_types(cls):
        names = []
        for genre in cls.__members__.values():
            names.append(genre.text)
        return names

    @classmethod
    def get_types_without_first(cls):
        names = []
        for genre in cls.__members__.values():
            names.append(genre.text)
        names.pop(0)
        return names
