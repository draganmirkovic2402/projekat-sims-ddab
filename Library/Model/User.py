from abc import ABC, abstractmethod
from datetime import datetime
from Library.Model.Enums import UserType
from Library.Application.config import DATE_FORMAT


class User(ABC):

    def __init__(self, info_dict):
        self._pk = info_dict["pk"]
        self._username = info_dict["username"]
        self._password = info_dict["password"]
        self._active = info_dict["active"]
        self._name = info_dict["name"]
        self._surname = info_dict["surname"]
        self._date_of_birth = datetime.strptime(info_dict["date_of_birth"], DATE_FORMAT)
        self._jmbg = info_dict["jmbg"]
        self._address = [info_dict["address"], None]
        self._email = info_dict["email"]
        self._user_type = UserType(info_dict["user_type"])

    @property
    def pk(self):
        return self._pk

    @property
    def username(self):
        return self._username

    @property
    def password(self):
        return self._password

    @property
    def active(self):
        return self._active

    @property
    def name(self):
        return self._name

    @property
    def surname(self):
        return self._surname

    @property
    def date_of_birth(self):
        return self._date_of_birth

    @property
    def jmbg(self):
        return self._jmbg

    @property
    def address_pk(self):
        return self._address[0]

    @property
    def address(self):
        return self._address[1]

    @property
    def email(self):
        return self._email

    @property
    def user_type(self):
        return self._user_type

    @address.setter
    def address(self, a):
        self._address[1] = a

    @password.setter
    def password(self, p):
        self._password = p

    def deactivate(self):
        self._active = False

    def activate(self):
        self._active = True

    def to_user_dict(self):
        user_dict = {
            "pk": self._pk,
            "username": self._username,
            "password": self._password,
            "active": self._active,
            "name": self._name,
            "surname": self._surname,
            "date_of_birth": self._date_of_birth.strftime(DATE_FORMAT),
            "jmbg": self._jmbg,
            "address": self._address[1].pk,
            "email": self._email,
            "user_type": self._user_type.value
        }
        return user_dict
