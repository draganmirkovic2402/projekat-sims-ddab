from PyQt5 import QtCore, QtGui, QtWidgets
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import ValidType
from Library.Application import config
from PyQt5.uic import loadUi
import os


class RentedBooksView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(RentedBooksView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager
        self._data = None

        loadUi(config.RENTED_BOOKS_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND1060x600.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.extendBtn.clicked.connect(self.extendBtnPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.extendBtn.setStyleSheet("background-image : url("+config.BUTTON_EXT_LOAN.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


        self.bookTable.setColumnCount(6)

        self.bookTable.setColumnWidth(0, 200)
        self.bookTable.setColumnWidth(1, 265)
        self.bookTable.setColumnWidth(2, 150)
        self.bookTable.setColumnWidth(3, 150)
        self.bookTable.setColumnWidth(4, 50)
        self.bookTable.setColumnWidth(5, 50)

        self.bookTable.horizontalHeader().setStretchLastSection(True)
        self.bookTable.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        self.load_data()

        model = ['PK', 'Naziv', 'Autor', 'Datum Pozajmice', 'Rok Vracanja', ' ']
        self.bookTable.hideColumn(0)
        self.bookTable.setHorizontalHeaderLabels(model)

    @property
    def widget(self):
        return self._widgetStack

    def load_data(self):
        self._data = self._controller.get_current_rented_books(self._user)
        self.bookTable.setRowCount(len(self._data))
        for i in range(len(self._data)):
            for j in range(5):
                item = QtWidgets.QTableWidgetItem(self._data[i][j])
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsSelectable)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.bookTable.setItem(i, j, item)

        for i in range(len(self._data)):
            chBox = QtWidgets.QTableWidgetItem("")
            chBox.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
            chBox.setCheckState(QtCore.Qt.Unchecked)
            self.bookTable.setItem(i, 5, chBox)

    def cancelButtonPressed(self):
        self.close()

    def extendBtnPressed(self):
        targeted_fields = []
        for i in range(self.bookTable.rowCount()):
            date_already_extended = self._data[i][5]
            if self.bookTable.item(i, 5).checkState() == QtCore.Qt.Checked and date_already_extended is False:
                targeted_fields.append(int(self.bookTable.item(i, 0).text()))

        retVal = self._controller.extend_return_date(self._user, targeted_fields)
        messageBox(retVal)
        if retVal.number > 0:
            self.close()
