from Library.View.UserView.PasswordChangeView import PasswordChangeView
from Library.View.MemberView.ExtendMembershipView import ExtendMembershipView
from Library.View.MemberView.RentedBooksView import RentedBooksView
from Library.View.MemberView.BookHistoryView import BookHistoryView
from Library.View.MemberView.ReservedBooksView import ReservedBooksView
from Library.View.MemberView.SearchReserveView import SearchReserveView
from datetime import datetime
from Library.Application.config import DATE_FORMAT
from Library.Application import config
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.uic import loadUi


class MemberMainView(QtWidgets.QMainWindow):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(MemberMainView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager

        loadUi(config.MEMBER_MAIN_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND1060x600.as_posix() + ")")
        self.setWindowTitle("Bibi-Član")
        full_name = user.name + " " + user.surname
        self.nameLbl.setText(full_name)
        self.dateOfBirthLbl.setText(datetime.strftime(user.date_of_birth, DATE_FORMAT))
        self.memTypeLbl.setText(user.status.text)
        self.dateOfMembLbl.setText(datetime.strftime(user.membership_date, DATE_FORMAT))

        self.logoutBtn.clicked.connect(self.logoutBtnPressed)
        self.extendMembershipBtn.clicked.connect(self.extendMembershipBtnPressed)
        self.passChangeBtn.clicked.connect(self.passChangeBtnPressed)
        self.reservedBooksBtn.clicked.connect(self.reservedBooksBtnPressed)
        self.bookPreviewBtn.clicked.connect(self.bookPreviewBtnPressed)
        self.historyPreviewBtn.clicked.connect(self.historyPreviewBtnPressed)
        self.rentedBooksBtn.clicked.connect(self.rentedBooksBtnPressed)

        self.logoutBtn.setStyleSheet("background-image : url("+config.BUTTON_LOGOUT.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.extendMembershipBtn.setStyleSheet("background-image : url("+config.BUTTON_EXT_MEMB.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.passChangeBtn.setStyleSheet("background-image : url("+config.BUTTON_CHANGE_PASS.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.reservedBooksBtn.setStyleSheet("background-image : url("+config.BUTTON_RES_MEMB.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.bookPreviewBtn.setStyleSheet("background-image : url("+config.BUTTON_BOOKS.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.historyPreviewBtn.setStyleSheet("background-image : url("+config.BUTTON_HISTORY.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.rentedBooksBtn.setStyleSheet("background-image : url("+config.BUTTON_RENTED_BOOKS.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


    @property
    def widget(self):
        return self._widgetStack

    def passChangeBtnPressed(self):
        passwordView = PasswordChangeView(self.widget, self._user, self._controller, self._lib_manager)
        passwordView.show()
        passwordView.exec_()

    def reservedBooksBtnPressed(self):
        reservedBooksView = ReservedBooksView(self.widget, self._user, self._controller,
                                              self._lib_manager)  # treba da se dobave podaci
        reservedBooksView.show()
        reservedBooksView.exec_()

    def bookPreviewBtnPressed(self):
        searchReserveView = SearchReserveView(self.widget, self._user, self._controller, self._lib_manager)
        searchReserveView.show()
        searchReserveView.exec_()

    def historyPreviewBtnPressed(self):
        bookHistoryView = BookHistoryView(self.widget, self._user, self._controller, self._lib_manager)
        bookHistoryView.show()
        bookHistoryView.exec_()

    def rentedBooksBtnPressed(self):
        rentedBooksView = RentedBooksView(self.widget, self._user, self._controller, self._lib_manager)
        rentedBooksView.show()
        rentedBooksView.exec_()

    def extendMembershipBtnPressed(self):
        extendMembershipView = ExtendMembershipView(self.widget, self._user, self._controller, self._lib_manager)
        extendMembershipView.show()
        extendMembershipView.exec_()
        self.dateOfMembLbl.setText(datetime.strftime(self._user.membership_date, DATE_FORMAT))

    def logoutBtnPressed(self):
        self.widget.setCurrentIndex(self.widget.currentIndex() - 1)
        self.widget.removeWidget(self)
