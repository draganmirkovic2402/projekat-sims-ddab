from PyQt5 import QtCore, QtGui, QtWidgets
from Library.Application import config
from PyQt5.uic import loadUi



class BookHistoryView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(BookHistoryView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager

        loadUi(config.MEMBER_BOOK_HISTORY_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND1060x600.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


        self.bookTable.setColumnCount(3)
        self.bookTable.horizontalHeader().setStretchLastSection(True)
        self.bookTable.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        self.load_data()

        model = ['Naziv', 'Autor', 'Datum Pozajmice']

        self.bookTable.setHorizontalHeaderLabels(model)

    @property
    def widget(self):
        return self._widgetStack

    def load_data(self):
        data = self._controller.get_all_rented_books(self._user)
        self.bookTable.setRowCount(len(data))
        for i in range(len(data)):
            for j in range(3):
                item = QtWidgets.QTableWidgetItem(data[i][j])
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsSelectable)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.bookTable.setItem(i, j, item)

    def cancelButtonPressed(self):
        self.close()
