from PyQt5 import QtCore, QtGui, QtWidgets
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import ValidType
from Library.Application import config
from PyQt5.uic import loadUi



class ReservedBooksView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(ReservedBooksView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager
        loadUi(config.RESERVED_BOOKS_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND1060x600.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.extendBtn.clicked.connect(self.cancelReservationBtnPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.extendBtn.setStyleSheet("background-image : url("+config.BUTTON_RES_CANCEL.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


        self.bookTable.setColumnCount(6)

        self.bookTable.setColumnWidth(0, 200)
        self.bookTable.setColumnWidth(1, 265)
        self.bookTable.setColumnWidth(2, 150)
        self.bookTable.setColumnWidth(3, 150)
        self.bookTable.setColumnWidth(4, 50)
        self.bookTable.setColumnWidth(5, 50)

        self.bookTable.horizontalHeader().setStretchLastSection(True)
        self.bookTable.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        self.load_data()

        model = ['PK', 'Naziv', 'Autor', 'Datum Rezervacije', 'Rok Preuzimanja', ' ']
        self.bookTable.hideColumn(0)
        self.bookTable.setHorizontalHeaderLabels(model)

    @property
    def widget(self):
        return self._widgetStack

    def load_data(self):
        data = self._controller.get_current_reserved_books(self._user)
        self.bookTable.setRowCount(len(data))
        for i in range(len(data)):
            for j in range(5):
                item = QtWidgets.QTableWidgetItem(data[i][j])
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsSelectable)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.bookTable.setItem(i, j, item)

        for i in range(len(data)):
            chBox = QtWidgets.QTableWidgetItem("")
            chBox.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
            chBox.setCheckState(QtCore.Qt.Unchecked)
            self.bookTable.setItem(i, 5, chBox)

    def cancelButtonPressed(self):
        self.close()

    def cancelReservationBtnPressed(self):
        targeted_fields = []
        for i in range(self.bookTable.rowCount()):
            if self.bookTable.item(i, 5).checkState() == QtCore.Qt.Checked:
                targeted_fields.append(int(self.bookTable.item(i, 0).text()))

        retVal = self._controller.cancel_reservation(self._user, targeted_fields)
        messageBox(retVal)
        if retVal.number > 0:
            self.close()
