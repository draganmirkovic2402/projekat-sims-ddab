from PyQt5 import QtCore, QtGui, QtWidgets

from Library.Controller.UserController import UserController
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import ValidType, Genre
from Library.Application import config
from PyQt5.uic import loadUi


class SearchReserveView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(SearchReserveView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager
        loadUi(config.SEARCH_RESERVE_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND1060x600.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelBtnPressed)
        self.reserveBtn.clicked.connect(self.reserveBtnPressed)
        self.searchBtn.clicked.connect(self.searchBtnPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.reserveBtn.setStyleSheet("background-image : url("+config.BUTTON_RES_MEMB_CONFIRM.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.searchBtn.setStyleSheet("background-image : url("+config.BUTTON_SEARCH.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


        self.bookTable.setColumnCount(9)

        self.bookTable.setColumnWidth(0, 30)
        self.bookTable.setColumnWidth(1, 130)
        self.bookTable.setColumnWidth(2, 200)
        self.bookTable.setColumnWidth(3, 150)
        self.bookTable.setColumnWidth(4, 150)
        self.bookTable.setColumnWidth(5, 150)
        self.bookTable.setColumnWidth(6, 200)
        self.bookTable.setColumnWidth(7, 30)
        self.bookTable.setColumnWidth(8, 120)

        self.genreCombo.addItems(Genre.get_types())

        self.load_data()

        model = ['PK', 'Naziv', 'Autor', 'Izdavac', 'Filijala', 'Godina izdanja', 'Broj slobodnih primjeraka', ' ', 'Broj primjeraka']
        self.bookTable.hideColumn(0)
        self.bookTable.setHorizontalHeaderLabels(model)

    @property
    def widget(self):
        return self._widgetStack

    def load_data(self):
        self._data = self._controller.get_all_books()
        self._fill_table_with_data()

    def load_searched_data(self, title, author, genre, keywords):
        self._data = self._controller.filter_data(title, author, genre, keywords)
        self._fill_table_with_data()

    def _fill_table_with_data(self):
        self.bookTable.setRowCount(len(self._data))
        for i in range(len(self._data)):
            for j in range(7):
                item = QtWidgets.QTableWidgetItem(self._data[i][j])
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsSelectable)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.bookTable.setItem(i, j, item)
        for i in range(len(self._data)):
            chBox = QtWidgets.QTableWidgetItem("")
            chBox.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
            chBox.setTextAlignment(QtCore.Qt.AlignCenter)
            chBox.setCheckState(QtCore.Qt.Unchecked)
            self.bookTable.setItem(i, 7, chBox)
            self.bookTable.setItem(i, 8, QtWidgets.QTableWidgetItem('0'))

    def cancelBtnPressed(self):
        self.close()

    def searchBtnPressed(self):
        title = self.titleTxt.text()
        author = self.authorTxt.text()
        genre = self.genreCombo.currentText()
        keywords = self.keyWordsTxt.toPlainText()
        self.load_searched_data(title, author, genre, keywords)

    def reserveBtnPressed(self):  # u 2prolaza prvo validacija pa onda dodavanja
        is_num_of_copies_valid = True
        books_and_num_of_copies = {}
        for i in range(len(self._data)):
            if self.bookTable.item(i, 7).checkState() == QtCore.Qt.Checked:
                is_num_of_copies_valid = UserController.is_num_of_reserved_copies_valid(self.bookTable.item(i, 8).text(), self._data[i][6])
        if is_num_of_copies_valid:
            for i in range(len(self._data)):
                if self.bookTable.item(i, 7).checkState() == QtCore.Qt.Checked:
                    books_and_num_of_copies[int(self.bookTable.item(i, 0).text())] = int(
                        self.bookTable.item(i, 8).text())
            retVal = self._controller.reserve_books(self._user, books_and_num_of_copies)
            messageBox(retVal)
            if retVal.number > 0:
                self.close()
        else:
            messageBox(ValidType.INV_COPIES)
