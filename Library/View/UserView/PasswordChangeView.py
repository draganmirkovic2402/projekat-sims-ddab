from PyQt5 import QtCore, QtGui, QtWidgets
from Library.View.UserView.MessageFrame import messageBox
from Library.Application import config
from Library.Model.Enums import ValidType
from PyQt5.uic import loadUi



class PasswordChangeView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(PasswordChangeView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager
        loadUi(config.PASSWORD_CHANGE_UI, self)

        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND500x500.as_posix() + ")")
        self.setWindowTitle("Izmena Lozinke")

        self.confirmBtn.clicked.connect(self.confirmBtnPressed)
        self.cancelBtn.clicked.connect(self.cancelBtnPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.confirmBtn.setStyleSheet("background-image : url("+config.BUTTON_CHANGE_PASS_CONFIRM.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


    @property
    def widget(self):
        return self._widgetStack

    def cancelBtnPressed(self):
        self.close()

    def confirmBtnPressed(self):
        oldPass = self.oldPassTxt.text()
        newPass = self.newPassTxt.text()
        newPassConfirm = self.newPassConfirmTxt.text()

        retVal = self._controller.change_password(self._user, oldPass, newPass, newPassConfirm)
        messageBox(retVal)
        if retVal.number>0:
            self.close()
