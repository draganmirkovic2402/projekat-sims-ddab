from PyQt5 import QtCore, QtGui, QtWidgets
from Library.Model.Enums import ValidType

def messageBox(valid_type):
    msg = QtWidgets.QMessageBox()
    msg.setIcon(QtWidgets.QMessageBox.Information)
    msg.setText(valid_type.text)
    msg.setWindowTitle(valid_type.title)
    msg.show()
    msg.exec_()
