from PyQt5 import QtCore, QtGui, QtWidgets
from Library.Application import config
from Library.View.EmployeeView.EmployeeMainView import EmployeeMainView
from Library.View.MemberView.MemberMainView import MemberMainView
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import Genre, ValidType
from PyQt5.uic import loadUiType, loadUi
from Library.Model.Enums import UserType


class LoginView(QtWidgets.QMainWindow):
    def __init__(self, stackedWidgetP, controller, lib_manager):
        super(LoginView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = None
        self._controller = controller
        self._lib_manager = lib_manager
        self._data = None

        loadUi(config.LOGIN_VIEW_UI, self)

        self.setWindowTitle("Prijava")
        self.passTxt.setEchoMode(QtWidgets.QLineEdit.Password)
        self.genreCombo.addItems(Genre.get_types())

        self.loginBtn.setStyleSheet("background-image : url("+config.BUTTON_LOGIN.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.searchBtn.setStyleSheet("background-image : url("+config.BUTTON_SEARCH.as_posix()+");background-position: center center;border: 1px solid antiquewhite")

        self.backgroundLbl.setStyleSheet("background-image : url("+config.BACKGROUND1060x600.as_posix()+")")
        # Listeneri
        self.loginBtn.clicked.connect(self.loginBtnPresed)
        self.searchBtn.clicked.connect(self.searchBtnPressed)
        # Table
        self.bookTable.setCornerButtonEnabled(False)
        self.bookTable.setColumnCount(6)

        self.bookTable.setColumnWidth(0, 20)
        self.bookTable.setColumnWidth(1, 265)
        self.bookTable.setColumnWidth(2, 150)
        self.bookTable.setColumnWidth(3, 150)
        self.bookTable.setColumnWidth(4, 50)
        self.bookTable.setColumnWidth(5, 50)

        self.bookTable.horizontalHeader().setStretchLastSection(True)
        self.bookTable.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        self.load_data()

        model = ['PK', 'Naziv', 'Autor', 'Izdavac', 'Filijala', 'Godina izdanja']
        self.bookTable.hideColumn(0)
        self.bookTable.setHorizontalHeaderLabels(model)

    @property
    def widget(self):
        return self._widgetStack

    def load_data(self):
        self._data = self._controller.get_all_books()
        self.bookTable.setRowCount(len(self._data))
        for i in range(len(self._data)):
            for j in range(6):
                item = QtWidgets.QTableWidgetItem(self._data[i][j])
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsSelectable)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.bookTable.setItem(i, j, item)

    def load_searched_data(self, title, author, genre, keywords):
        data = self._controller.filter_data(title, author, genre, keywords)
        self.bookTable.setRowCount(len(data))
        for i in range(len(data)):
            for j in range(6):
                item = QtWidgets.QTableWidgetItem(data[i][j])
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsSelectable)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.bookTable.setItem(i, j, item)

    def loginBtnPresed(self):
        userName = self.userTxt.text()
        print(type(self.userTxt))
        userPass = self.passTxt.text()

        self._user = self._controller.get_user_from_login(userName, userPass)

        if self._user is not None:
            messageBox(ValidType.LOGIN_SUCC)
            if self._user.user_type == UserType.EMPLOYEE:
                employeeMainView = EmployeeMainView(self.widget, self._user, self._controller, self._lib_manager)
                self.widget.addWidget(employeeMainView)
                self.widget.setCurrentIndex(self.widget.currentIndex() + 1)
            else:
                memberMainView = MemberMainView(self.widget, self._user, self._controller, self._lib_manager)
                self.widget.addWidget(memberMainView)
                self.widget.setCurrentIndex(self.widget.currentIndex() + 1)
        else:
            messageBox(ValidType.LOGIN_NOT_SUCC)

    def searchBtnPressed(self):
        title = self.titleTxt.text()
        author = self.authorTxt.text()
        genre = self.genreCombo.currentText()
        keywords = self.keyWordsTxt.toPlainText()
        self.load_searched_data(title, author, genre, keywords)
