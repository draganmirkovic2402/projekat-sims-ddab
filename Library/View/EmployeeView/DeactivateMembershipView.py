from PyQt5 import QtCore, QtGui, QtWidgets
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import ValidType
from Library.Application import config
from PyQt5.uic import loadUi



class DeactivateMembershipView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(DeactivateMembershipView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager

        loadUi(config.DEACTIVATE_MEMBER_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND500x500.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.deactivateBtn.clicked.connect(self.deactivateBtnPressed)
        self.activateBtn.clicked.connect(self.activateBtnPressed)

        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.deactivateBtn.setStyleSheet("background-image : url("+config.BUTTON_DEACTIVATE.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.activateBtn.setStyleSheet("background-image : url("+config.BUTTON_ACTIVATE.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


    @property
    def widget(self):
        return self._widgetStack

    def cancelButtonPressed(self):
        self.close()

    def activateBtnPressed(self):
        username = self.usernameTxt.text()
        print(type(self.usernameTxt))
        ret_val = self._controller.activate_membership(username)
        messageBox(ret_val)
        if ret_val.number > 0:
            self.close()

    def deactivateBtnPressed(self):
        username = self.usernameTxt.text()
        ret_val = self._controller.deactivate_membership(username)
        messageBox(ret_val)
        if ret_val.number > 0:
            self.close()
