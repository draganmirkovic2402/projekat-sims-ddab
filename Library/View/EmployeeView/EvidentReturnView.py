from PyQt5 import QtCore, QtGui, QtWidgets
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import ValidType
from Library.Application import config
from PyQt5.uic import loadUi



class EvidentReturnView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(EvidentReturnView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager

        loadUi(config.EVIDENT_RETURN_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND500x500.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.evidentBtn.clicked.connect(self.evidentBtnPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.evidentBtn.setStyleSheet("background-image : url("+config.BUTTON_EVIDENT_RETURN.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


    @property
    def widget(self):
        return self._widgetStack

    def cancelButtonPressed(self):
        self.close()

    def evidentBtnPressed(self):

        rented_copies = self.rentIdTxt.text()
        ret_val = self._controller.evident_return(rented_copies, self._user)
        messageBox(ret_val)
        if ret_val.number > 0:
            self.close()