from datetime import datetime
from PyQt5 import QtCore, QtGui, QtWidgets
from dateutil.relativedelta import relativedelta
from Library.Application.config import DATE_FORMAT
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import MemberStatus, MembershipType,ValidType
from Library.Application import config
from PyQt5.uic import loadUi



class AddMemberView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(AddMemberView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager

        loadUi(config.ADD_MEMBER_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND500x1000.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.addMemberBtn.clicked.connect(self.addMemberBtnPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.addMemberBtn.setStyleSheet("background-image : url("+config.BUTTON_ADD_MEMB.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


        self.roleCombo.addItems(MemberStatus.get_types())
        membershipTypes = ["Pola godine", "Godinu dana"]
        self.membershipCombo.addItems(membershipTypes)

    @property
    def widget(self):
        return self._widgetStack

    def cancelButtonPressed(self):
        self.close()

    def addMemberBtnPressed(self):

        user_info = {}
        user_info["username"] = self.usernameTxt.text()
        user_info["password"] = self.passTxt.text()
        user_info["name"] = self.nameTxt.text()
        user_info["surname"] = self.lastnameTxt.text()
        user_info["date_of_birth"] = self.dateOfBirthTxt.text()
        user_info["jmbg"] = self.jmbgTxt.text()
        user_info["email"] = self.mailTxt.text()
        user_info["street"] = self.streetTxt.text()
        user_info["number"] = self.numberTxt.text()
        user_info["place"] = self.placeNameTxt.text()
        user_info["postal_code"] = self.pttTxt.text()
        user_info["status"] = MemberStatus.parse_text(self.roleCombo.currentText())
        membership = MembershipType.parse_text(self.membershipCombo.currentText())
        membership_date = datetime.now() + relativedelta(months=membership.months)
        user_info["membership_date"] = datetime.strftime(membership_date, DATE_FORMAT)
        return_value = self._controller.register_member(user_info)
        messageBox(return_value)
        if return_value.number > 0:
            self.close()
