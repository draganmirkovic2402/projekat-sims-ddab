from PyQt5 import QtCore, QtGui, QtWidgets
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import ValidType
from Library.Application import config
from PyQt5.uic import loadUi



class ExtendMembershipView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(ExtendMembershipView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager

        loadUi(config.EXTEND_MEMB_EMPLOYEE_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND1060x600.as_posix() + ")")

        membershipTypes = ["Pola godine", "Godinu dana"]
        self.membershipCombo.addItems(membershipTypes)

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.extendMemBtn.clicked.connect(self.extendMemBtnPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.extendMemBtn.setStyleSheet("background-image : url("+config.BUTTON_EXT_MEMB_CONFIRM.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


    @property
    def widget(self):
        return self._widgetStack

    def cancelButtonPressed(self):
        self.close()

    def extendMemBtnPressed(self):
        username = self.usernameTxt.text()
        membType = self.membershipCombo.currentText()
        retVal = self._controller.extend_membership(username, membType)
        messageBox(retVal)
        if retVal.number > 0:
            self.close()
