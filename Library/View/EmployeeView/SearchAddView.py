from PyQt5 import QtCore, QtGui, QtWidgets

from Library.Model.Enums import Genre, ValidType
from Library.View.UserView.MessageFrame import messageBox
from Library.View.EmployeeView.AddBookView import AddBookView
from Library.View.EmployeeView.AddCopiesView import AddCopiesView
from Library.View.EmployeeView.EvidentLoanView import EvidentLoanView
from Library.Application import config
from PyQt5.uic import loadUi


class SearchAddView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(SearchAddView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager
        self._data = None

        loadUi(config.SEARCH_ADD_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND1060x600.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelBtnPressed)
        self.addCopyBtn.clicked.connect(self.addCopyBtnPressed)
        self.searchBtn.clicked.connect(self.searchBtnPressed)
        self.addBookBtn.clicked.connect(self.addBookBtnPressed)
        self.loanBtn.clicked.connect(self.loanBtnPressed)

        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.addCopyBtn.setStyleSheet("background-image : url("+config.BUTTON_ADD_COPIES.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.searchBtn.setStyleSheet("background-image : url("+config.BUTTON_SEARCH.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.addBookBtn.setStyleSheet("background-image : url("+config.BUTTON_ADD_BOOK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.loanBtn.setStyleSheet("background-image : url("+config.BUTTON_LOAN.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


        self.bookTable.setColumnCount(6)
        self.bookTable.horizontalHeader().setStretchLastSection(True)
        self.bookTable.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        self.genreCombo.addItems(Genre.get_types())

        self.load_data()

        model = ['PK', 'Naziv', 'Autor', 'Izdavac', 'Filijala', 'Godina izdanja']
        self.bookTable.hideColumn(0)
        self.bookTable.setHorizontalHeaderLabels(model)

    @property
    def widget(self):
        return self._widgetStack

    def load_data(self):
        self._data = self._controller.get_branch_office_books(self._user.workplace_pk)
        self.bookTable.setRowCount(len(self._data))
        for i in range(len(self._data)):
            for j in range(6):
                item = QtWidgets.QTableWidgetItem(self._data[i][j])
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsSelectable)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.bookTable.setItem(i, j, item)

    def load_searched_data(self, title, author, genre, keywords):
        data = self._controller.filter_data(title, author, genre, keywords)
        self.bookTable.setRowCount(len(data))
        for i in range(len(data)):
            for j in range(6):
                item = QtWidgets.QTableWidgetItem(data[i][j])
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsSelectable)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.bookTable.setItem(i, j, item)

    def cancelBtnPressed(self):
        self.close()

    def addBookBtnPressed(self):
        addBookView = AddBookView(self.widget, self._user, self._controller, self._lib_manager)
        addBookView.show()
        addBookView.exec_()
        self.load_data()

    def loanBtnPressed(self):
        current_row = self.bookTable.currentRow()
        if current_row != -1:
            selected_row = int(self.bookTable.item(current_row, 0).text())
            evidentLoanView = EvidentLoanView(self.widget, self._user, self._controller, self._lib_manager, selected_row)
            evidentLoanView.show()
            evidentLoanView.exec_()
        else:
            messageBox(ValidType.BOOK_NOT_SEL)

    def searchBtnPressed(self):
        title = self.titleTxt.text()
        author = self.authorTxt.text()
        genre = self.genreCombo.currentText()
        keywords = self.keyWordsTxt.toPlainText()
        self.load_searched_data(title, author, genre, keywords)

    def addCopyBtnPressed(self):
        current_row = self.bookTable.currentRow()
        if current_row != -1:
            selected_row = int(self.bookTable.item(current_row, 0).text())
            addCopiesView = AddCopiesView(self.widget, self._user, self._controller, self._lib_manager, selected_row)
            addCopiesView.show()
            addCopiesView.exec_()
        else:
            messageBox(ValidType.BOOK_NOT_SEL)
