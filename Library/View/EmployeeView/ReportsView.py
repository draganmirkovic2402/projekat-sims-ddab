from PyQt5 import QtCore, QtGui, QtWidgets

from Library.Application import config
from PyQt5.uic import loadUi



class ReporstView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(ReporstView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager
        loadUi(config.REPORTS_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND500x500.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


    @property
    def widget(self):
        return self._widgetStack

    def cancelButtonPressed(self):
        self.close()
