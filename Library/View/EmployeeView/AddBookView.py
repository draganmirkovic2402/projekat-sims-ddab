from PyQt5 import QtCore, QtGui, QtWidgets
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import Genre, ValidType
from Library.Application import config
from PyQt5.uic import loadUi



class AddBookView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(AddBookView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager

        loadUi(config.ADD_BOOK_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND1000x800.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.addBookBtn.clicked.connect(self.addBookBtnPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.addBookBtn.setStyleSheet("background-image : url("+config.BUTTON_ADD_BOOK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")



        self.rentableCombo.addItems(["Da", "Ne"])
        self.genreCombo.addItems(Genre.get_types_without_first())

    @property
    def widget(self):
        return self._widgetStack

    def cancelButtonPressed(self):
        self.close()

    def addBookBtnPressed(self):
        book_info = {}
        book_info["authors"] = self.authorTxt.text().split(",")
        book_info["title"] = self.titleTxt.text()
        book_info["publisher"] = self.publisherTxt.text()
        book_info["publishing_year"] = self.publishingYearTxt.text()
        book_info["book_format"] = self.formatTxt.text()
        genre_string = self.genreCombo.currentText()
        book_info["genre"] = Genre.parse_text(genre_string)
        book_info["key_words"] = self.keyWordsTxtEdit.toPlainText().split(",")
        book_info["additional_notes"] = self.additionalNotesTxt.toPlainText()
        book_info["isbn"] = self.isbnTxt.text()
        if self.rentableCombo.currentText() == "Da":
            book_info["is_rentable"] = True
        else:
            book_info["is_rentable"] = False
        book_info["num_copies"] = self.numOfCopies.text()
        ret_val = self._controller.add_book(book_info, self._user)
        messageBox(ret_val)
        if ret_val.number > 0:
            self.close()
