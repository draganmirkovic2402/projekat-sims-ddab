from PyQt5 import QtCore, QtGui, QtWidgets
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import ValidType
from Library.Application import config
from PyQt5.uic import loadUi



class AddCopiesView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager, book_copies):
        super(AddCopiesView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager
        self._book_copies = book_copies

        loadUi(config.ADD_COPIES_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND500x500.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.addBtn.clicked.connect(self.addBtnPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.addBtn.setStyleSheet("background-image : url("+config.BUTTON_ADD_COPIES.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


    @property
    def widget(self):
        return self._widgetStack

    @property
    def book_copies(self):
        return self._book_copies

    @book_copies.setter
    def book_copies(self, book_copies):
        self._book_copies = book_copies

    def cancelButtonPressed(self):
        self.close()

    def addBtnPressed(self):
        num_copies = self.copiesTxt.text()
        ret_val = self._controller.add_copies(self.book_copies, num_copies)
        messageBox(ret_val)
        if ret_val.number > 0:
            self.close()
