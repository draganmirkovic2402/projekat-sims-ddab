from PyQt5 import QtCore, QtGui, QtWidgets
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import EmployeeStatus,ValidType
from Library.Application import config
from PyQt5.uic import loadUi



class AddEmployeeView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(AddEmployeeView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager

        loadUi(config.ADD_EMPLOYEE_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND1000x800.as_posix() + ")")

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.addEmployeeBtn.clicked.connect(self.addEmployeeBtnPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.addEmployeeBtn.setStyleSheet("background-image : url("+config.BUTTON_ADD_EMP_CONFIRM.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


        self.roleCombo.addItems(EmployeeStatus.get_types())
        self.workPlaceTable.setColumnCount(2)
        self.workPlaceTable.horizontalHeader().setStretchLastSection(True)
        self.workPlaceTable.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        self.load_branch_offices()

        model = ['PK', 'Filijala']
        self.workPlaceTable.hideColumn(0)
        self.workPlaceTable.setHorizontalHeaderLabels(model)

    def load_branch_offices(self):
        data = self._controller.get_branch_offices()
        self.workPlaceTable.setRowCount(len(data))
        for i in range(len(data)):
            for j in range(2):
                item = QtWidgets.QTableWidgetItem(str(data[i][j]))
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsSelectable)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                self.workPlaceTable.setItem(i, j, item)

    @property
    def widget(self):
        return self._widgetStack

    def cancelButtonPressed(self):
        self.close()

    def addEmployeeBtnPressed(self):

        user_info = {}
        user_info["username"] = self.usernameTxt.text()
        user_info["password"] = self.passTxt.text()
        user_info["name"] = self.nameTxt.text()
        user_info["surname"] = self.lastnameTxt.text()
        user_info["date_of_birth"] = self.dateOfBirthTxt.text()
        user_info["jmbg"] = self.jmbgTxt.text()
        user_info["email"] = self.mailTxt.text()
        user_info["street"] = self.streetTxt.text()
        user_info["number"] = self.numberTxt.text()
        user_info["place"] = self.placeNameTxt.text()
        user_info["postal_code"] = self.pttTxt.text()
        user_info["status"] = EmployeeStatus.parse_text(self.roleCombo.currentText())
        current_row = self.workPlaceTable.currentRow()
        if current_row != -1:
            user_info["workplace"] = int(self.workPlaceTable.item(current_row, 0).text())
            return_value = self._controller.register_employee(user_info)
            messageBox(return_value)
            if return_value.number > 0:
                self.close()
        else:
            messageBox(ValidType.BRANCH_NOT_SEL)

