from PyQt5 import QtCore, QtGui, QtWidgets
from Library.View.UserView.MessageFrame import messageBox
from Library.Model.Enums import ValidType
from Library.Application import config
from PyQt5.uic import loadUi


class EvidentLoanView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager, book_copies):
        super(EvidentLoanView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager
        self._book_copies = book_copies
        # lib_manager.get_book_copies_number(book_copies)
        loadUi(config.EVIDENT_LOAN_UI, self)
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND500x500.as_posix() + ")")

        self.copiesLbl.setText(str(lib_manager.get_book_copies_number(book_copies)))

        self.cancelBtn.clicked.connect(self.cancelButtonPressed)
        self.evidentBtn.clicked.connect(self.evidentBtnPressed)
        self.cancelBtn.setStyleSheet("background-image : url("+config.BUTTON_BACK.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.evidentBtn.setStyleSheet("background-image : url("+config.BUTTON_LOAN.as_posix()+");background-position: center center;border: 1px solid antiquewhite")


    @property
    def widget(self):
        return self._widgetStack

    @property
    def book_copies(self):
        return self._book_copies

    @book_copies.setter
    def book_copies(self, book_copies):
        self._book_copies = book_copies

    def cancelButtonPressed(self):
        self.close()

    def evidentBtnPressed(self):
        username = self.usernameTxt.text()
        num_copies = self.numOfCopiesTxt.text()
        ret_val = self._controller.evident_loan(username, self.book_copies, num_copies)
        messageBox(ret_val)
        if ret_val.number > 0:
            self.close()
