from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.uic import loadUi
from Library.Application import config
from Library.View.UserView.PasswordChangeView import PasswordChangeView
from Library.View.EmployeeView.AddEmployeeView import AddEmployeeView
from Library.View.EmployeeView.AddMemberView import AddMemberView
from Library.View.EmployeeView.EvidentLoanView import EvidentLoanView
from Library.View.EmployeeView.EvidentReturnView import EvidentReturnView
from Library.View.EmployeeView.ExtendMembershipView import ExtendMembershipView
from Library.View.EmployeeView.DeactivateMembershipView import DeactivateMembershipView
from Library.View.EmployeeView.ReservedBooksView import ReservedBooksView
from Library.View.EmployeeView.SearchAddView import SearchAddView
from Library.View.EmployeeView.ReportsView import ReporstView
from Library.Model.Enums import EmployeeStatus


class EmployeeMainView(QtWidgets.QDialog):
    def __init__(self, stackedWidgetP, user, controller, lib_manager):
        super(EmployeeMainView, self).__init__()
        self._widgetStack = stackedWidgetP
        self._user = user
        self._controller = controller
        self._lib_manager = lib_manager

        loadUi(config.EMPLOYEE_MAIN_UI, self)

        self.setWindowTitle("Zaposleni")
        self.backgroundLbl.setStyleSheet("background-image : url(" + config.BACKGROUND1060x600.as_posix() + ")")

        full_name = user.name + " " + user.surname
        self.fullNameLbl.setText(full_name)

        self.logoutBtn.clicked.connect(self.logoutBtnPressed)
        self.changePassBtn.clicked.connect(self.changePassBtnPressed)
        self.regBtn.clicked.connect(self.regBtnPressed)
        self.extendMemBtn.clicked.connect(self.extendMemBtnPressed)
        self.deactivateBtn.clicked.connect(self.deactivateBtnPressed)
        self.addEmployeeBtn.clicked.connect(self.addEmployeeBtnPressed)
        self.bookReturnBtn.clicked.connect(self.bookReturnBtnPressed)
        self.viewBooksBtn.clicked.connect(self.viewBooksBtnPressed)
        self.reservationsViewBtn.clicked.connect(self.reservationsViewBtnPressed)
        self.reportsBtn.clicked.connect(self.reportsBtnPressed)
        self.reviewsViewBtn.clicked.connect(self.reviewsViewBtnPressed)
        self.priceListBtn.clicked.connect(self.priceListBtnPressed)

        self.logoutBtn.setStyleSheet("background-image : url("+config.BUTTON_LOGOUT.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.changePassBtn.setStyleSheet("background-image : url("+config.BUTTON_CHANGE_PASS.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.regBtn.setStyleSheet("background-image : url("+config.BUTTON_ADD_MEMB.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.extendMemBtn.setStyleSheet("background-image : url("+config.BUTTON_EXT_MEMB.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.deactivateBtn.setStyleSheet("background-image : url("+config.BUTTON_DEACTIAVTE_MENU.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.addEmployeeBtn.setStyleSheet("background-image : url("+config.BUTTON_ADD_EMP.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.bookReturnBtn.setStyleSheet("background-image : url("+config.BUTTON_EVIDENT_RETURN.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.viewBooksBtn.setStyleSheet("background-image : url("+config.BUTTON_BOOKS.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.reservationsViewBtn.setStyleSheet("background-image : url("+config.BUTTON_RES_EMP.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.reportsBtn.setStyleSheet("background-image : url("+config.BUTTON_REPORTS.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.reviewsViewBtn.setStyleSheet("background-image : url("+config.BUTTON_REVIEWS.as_posix()+");background-position: center center;border: 1px solid antiquewhite")
        self.priceListBtn.setStyleSheet("background-image : url("+config.BUTTON_PRICE_LIST.as_posix()+");background-position: center center;border: 1px solid antiquewhite")



        if user.status == EmployeeStatus.COUNTER_EMP or user.status == EmployeeStatus.PROCESS_EMP:
            self.priceListBtn.setEnabled(False)
            self.priceListBtn.setVisible(False)
            self.addEmployeeBtn.setEnabled(False)
            self.addEmployeeBtn.setVisible(False)
            self.reviewsViewBtn.setEnabled(False)
            self.reviewsViewBtn.setVisible(False)
        elif user.status == EmployeeStatus.MODERATOR:
            self.priceListBtn.setEnabled(False)
            self.priceListBtn.setVisible(False)
            self.addEmployeeBtn.setEnabled(False)
            self.addEmployeeBtn.setVisible(False)

    @property
    def widget(self):
        return self._widgetStack

    def logoutBtnPressed(self):
        self.widget.setCurrentIndex(self.widget.currentIndex() - 1)
        self.widget.removeWidget(self)

    def changePassBtnPressed(self):
        passwordView = PasswordChangeView(self.widget, self._user, self._controller, self._lib_manager)
        passwordView.show()
        passwordView.exec_()

    def regBtnPressed(self):
        addMemberView = AddMemberView(self.widget, self._user, self._controller, self._lib_manager)
        addMemberView.show()
        addMemberView.exec_()

    def extendMemBtnPressed(self):
        extendMembershipView = ExtendMembershipView(self.widget, self._user, self._controller, self._lib_manager)
        extendMembershipView.show()
        extendMembershipView.exec_()

    def deactivateBtnPressed(self):
        deactivateMembershipView = DeactivateMembershipView(self.widget, self._user, self._controller,
                                                            self._lib_manager)
        deactivateMembershipView.show()
        deactivateMembershipView.exec_()

    def addEmployeeBtnPressed(self):
        addEmplyeeView = AddEmployeeView(self.widget, self._user, self._controller, self._lib_manager)
        addEmplyeeView.show()
        addEmplyeeView.exec_()

    def bookReturnBtnPressed(self):
        evidentReturnView = EvidentReturnView(self.widget, self._user, self._controller, self._lib_manager)
        evidentReturnView.show()
        evidentReturnView.exec_()

    def viewBooksBtnPressed(self):
        searchAddView = SearchAddView(self.widget, self._user, self._controller, self._lib_manager)
        searchAddView.show()
        searchAddView.exec_()

    def reservationsViewBtnPressed(self):
        reservedBooksView = ReservedBooksView(self.widget, self._user, self._controller, self._lib_manager)
        reservedBooksView.show()
        reservedBooksView.exec_()

    def reportsBtnPressed(self):
        reporstView = ReporstView(self.widget, self._user, self._controller, self._lib_manager)
        reporstView.show()
        reporstView.exec_()


    def reviewsViewBtnPressed(self):
        print("review")

    def priceListBtnPressed(self):
        print("price list")
